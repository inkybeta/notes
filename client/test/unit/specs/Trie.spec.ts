import { Trie } from "@/text/parser/aho/Trie"
import { expect, should, use } from "chai";
import * as sc from "sinon-chai";
use(sc);

describe("Trie", function () {
    let t: Trie<string>;
    it("should be able to be constructed", function () {
        t = new Trie<string>();
    });

    it("should be able to add string", () => {
        t.add("hello", "item", true);
        t.add("help", "item", false);
    });


    it("should be able to find strings that were added", function() {
        function contains(word: string) {
            let state = t;
            for (let i = 0; i < word.length; i++) {
                if (!state.children.has(word[i])) {
                    console.log(state.children);
                    return false;
                }
                state = state.children.get(word[i])!;
            }
            return true;
        }
        expect(contains("hello")).to.be.true;
        expect(contains("help")).to.be.true;
    });
});