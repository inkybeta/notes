// This is a karma config file. For more details see
//   http://karma-runner.github.io/0.13/config/configuration-file.html
// we are also using it with karma-webpack
//   https://github.com/webpack/karma-webpack

var webpackConfig = require('../../build/webpack.test.conf')
var path = require("path");

module.exports = function karmaConfig(config) {
  config.set({
    basePath: __dirname,
    // to run in additional browsers:
    // 1. install corresponding karma launcher
    //    http://karma-runner.github.io/0.13/config/browsers.html
    // 2. add it to the `browsers` array below.
    browsers: ['PhantomJS'],
    frameworks: ['mocha', 'sinon-chai', 'phantomjs-shim', "karma-typescript"],
    reporters: ['spec', 'coverage'],
    files: [{ pattern: './specs/**/*.ts' }],
    preprocessors: {
      './index.ts': ['webpack', 'sourcemap'],
      'specs/**/*.ts': ['karma-typescript']
    },
    karmaTypescriptConfig: {
      tsconfig: '../../tsconfig.json'
    },
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    },
    coverageReporter: {
      dir: './coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    }
  })
}
