var webpackConfig = require('../../build/webpack.test.conf')

module.exports = function (config) {
    config.set({
        browsers: ['PhantomJS', 'Chrome'],
        basePath: __dirname,
        frameworks: ['mocha', 'sinon-chai', 'phantomjs-shim'],
        files: [
            'specs/**/*.spec.ts'
        ],
        preprocessors: {
            'specs/**/*.spec.ts': ['webpack', 'sourcemap']
        },
        webpack: webpackConfig,
        webpackServer: { noInfo: false },
        reporters: ['spec', 'coverage'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,

        karmaTypescriptConfig: {
            tsconfig: '../../tsconfig.json'
        },
        mime: {
            'text/x-typescript': ['ts', 'tsx'],
            'text/html': ['html']
        },
        coverageReporter: {
            dir: './coverage',
            reporters: [
                { type: 'lcov', subdir: '.' },
                { type: 'text-summary' }
            ]
        },
        autoWatch: true,
        singleRun: false
    })
}