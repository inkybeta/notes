let development = true;
if (process.env.NODE_ENV == 'development')
    development = true;
else if (process.env.NODE_ENV == 'production')
    development = false;
export default development;