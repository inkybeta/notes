import Vue from "vue";
import Router from "vue-router";
const HomePageVue = () => import("@/components/main/HomePage.vue");
const Main = () => import("@/components/main/MainPage.vue");
const Navigation = () => import("@/components/nav/Navigation.vue");
const EditPageVue = () => import("@/components/edit/EditPage.vue");
const UserManagement = () => import("@/components/auth/UserManagement.vue");
const IFrameMirrorVue = () =>
  import("@/components/edit/editor/standard/IFrameMirror.vue");
// const EmbedVue = () => import("@/components/mdraw/Embed.vue");
const AboutVue = () => import("@/components/nav/About.vue");
const QuackVue = () => import("@/components/utilities/Quack.vue");
Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: HomePageVue
    },
    {
      path: "/main/:path?",
      component: Main
    },
    {
      path: "/edit",
      component: EditPageVue
    },
    {
      path: "/navigation",
      component: Navigation
    },
    {
      path: "/user",
      component: UserManagement
    },
    {
      path: "/mirror",
      component: IFrameMirrorVue
    },
    // {
    //   path: "/mdraw",
    //   component: EmbedVue
    // },
    { path: "/about", component: AboutVue },
    { path: "/debug", component: QuackVue },
    { path: "/rubberduck", component: QuackVue }
  ]
});

export default router;
