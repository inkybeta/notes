import development from "@/development";

function getBaseUrl(): string {
    if (development) {
        return "http://localhost:8000"
    }
    return "";
}

const regex = /^((?:\/[a-z0-9]+)*)\/([a-z0-9]*)\/*$/

export class DirectoryIdentifier {
    public path!: string;
    public name!: string;

    public static parse(path: string): DirectoryIdentifier | null {
        let directory = new DirectoryIdentifier();
        let cap = regex.exec(path);
        if (cap == null) {
            return null;
        }
        let rPath = cap[1];
        if (rPath == "")
            rPath = "/";
        else
            rPath = rPath + "/";
        let name = cap[2];
        directory.path = rPath;
        directory.name = name;
        return directory;
    }

    public toString(): string {
        if (this.name == "") {
            return this.path;
        }
        return this.path + this.name + "/";
    }
}

export { getBaseUrl };