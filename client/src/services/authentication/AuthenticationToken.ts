export enum AuthenticationType {
    FIREBASE = 1

}

export class AuthenticationToken {
    public token!: string | null;
    public authenticationType!: number;
    constructor(authenticationType: AuthenticationType = AuthenticationType.FIREBASE, token: string | null = null) {
        this.token = token;
        this.authenticationType = authenticationType;
    }

    setToken(token: string) {
        this.token = token;
    }

    clearToken() {
        this.token = null;
    }

    getToken() {
        return this.token;
    }

    isLoggedIn() {
        return this.token != null;
    }
}