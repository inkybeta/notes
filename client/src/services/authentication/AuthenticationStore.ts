import { initializeApp, app } from "firebase/app";
import { AuthenticationToken, AuthenticationType } from "@/services/authentication/AuthenticationToken";
import { AuthenticationError, AuthenticationErrorType } from "@/services/errors/AuthenticationError"
import Axios from "axios";
import User from "@/models/auth/User";
import { getBaseUrl } from "..";
import * as firebase from "firebase/app";

require('firebase/auth');

export class AuthenticationStore {
    private app!: app.App;
    private firebaseLoaded!: boolean;
    private token!: string | null;

    constructor() {
        this.firebaseLoaded = false;
    }

    configure(config: any) {
        this.app = initializeApp(config);
        let app = this.app;
    }

    mapUrl(url: string) {
        return getBaseUrl() + url;
    }


    private getApp(): app.App {
        return <app.App>this.app;
    }

    async loginWithEmailAndPassword(email: string, password: string): Promise<string | null> {
        try {
            await this.getApp().auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
            await this.getApp().auth().signInWithEmailAndPassword(email, password);
        } catch (err) {
            var code = err.code;
            var message = err.message;
            if (code === "auth/invalid-email") {
                throw new AuthenticationError(AuthenticationErrorType.EMAIL_INVALID, message);
            } else if (code === "auth/user-disabled") {
                throw new AuthenticationError(AuthenticationErrorType.USER_DISABLED, message);
            } else if (code === "auth/user-not-found") {
                throw new AuthenticationError(AuthenticationErrorType.USER_NOT_FOUND, message);
            } else if (code === "auth/wrong-password") {
                throw new AuthenticationError(AuthenticationErrorType.WRONG_PASSWORD, message);
            } else {
                throw new AuthenticationError(AuthenticationErrorType.UNKNOWN, message);
            }
        }
        let user = null;
        if ((user = this.getApp().auth().currentUser) == null)
            return null;
        else
            return await user.getIdToken(true);

    }

    async register(user: User, password: string) {
        let result = await Axios.post(this.mapUrl("/api/user"), {
            "email": user.email,
            "first_name": user.firstName,
            "last_name": user.lastName,
            "password": password,
            "authentication_type": "Firebase"
        });
        if (result.status == 200)
            return result.data;
        return null;
    }

    async getUser(authenticationToken: AuthenticationToken): Promise<User> {
        let stored = localStorage.getItem("user");
        if (stored) {
            let d = JSON.parse(stored).payload;
            if (d == null || d["user_id"] == null) {
                localStorage.removeItem("user");
            } else {
                return new User(d["user_id"], d["email"], d["first_name"], d["last_name"], d["authentication_type"]);
            }
        }
        let result = await Axios.get(this.mapUrl("/api/user"), {
            headers: {
                "Authorization": "Firebase " + authenticationToken.getToken()
            }
        });
        if (result.status == 200) {
            let data = result.data;
            localStorage.setItem("user", JSON.stringify(data));
            let d = data.payload;
            return new User(d["user_id"], d["email"], d["first_name"], d["last_name"], d["authentication_type"]);
        }
        return new User();
    }

    async getCurrentToken(): Promise<AuthenticationToken> {
        let user = this.app.auth().currentUser;
        if (user != null){
            return new AuthenticationToken(AuthenticationType.FIREBASE, await user.getIdToken());
        }
        if (this.firebaseLoaded && this.token != undefined) {
            return new AuthenticationToken(AuthenticationType.FIREBASE, this.token);
        } else if (this.firebaseLoaded) {
            return new Promise<AuthenticationToken>((resolve) => {
                let count = 0;
                let interval = setInterval(() => {
                    count++;
                    if (count > 10 || this.token) {
                        clearInterval(interval);
                        let token = this.token || null;
                        resolve(new AuthenticationToken(AuthenticationType.FIREBASE, token));
                    }
                }, 250);
            })
        } else {
            let app = this.app;
            let auth = this;
            auth.firebaseLoaded = true;
            return await new Promise<AuthenticationToken>((resolve) => {
                app.auth().onAuthStateChanged(user => {
                    if (user) {
                        return user.getIdToken().then(t => {
                            auth.firebaseLoaded = true;
                            auth.token = t;
                            resolve(new AuthenticationToken(AuthenticationType.FIREBASE, t));
                        });
                    } else {
                        auth.firebaseLoaded = true;
                        auth.token = null;
                        return new AuthenticationToken();
                    }
                });
            });
        }
    }

    async refreshToken() {
        if (this.app != null) {
            let user = this.app.auth().currentUser;
            if (!user) return;
            return new AuthenticationToken(AuthenticationType.FIREBASE, await user.getIdToken(true));
        }
    }

    async logout(authenticationToken: AuthenticationToken) {
        try {
            await this.getApp().auth().signOut();
            localStorage.clear();
        } catch (err) {

        }
    }
}