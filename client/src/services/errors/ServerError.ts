export default class ServerError extends Error {
    constructor(msg: string = "The server made a mistake... Try again later") {
        super(msg);
    }
}

export class ItemMissingError extends ServerError {
    constructor() {
        super("The item was not located on server.")
    }
}