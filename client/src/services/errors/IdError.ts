export default class IdError extends Error {
  constructor(message?: string) {
    super(message);
  }
}
