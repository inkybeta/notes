
export class AuthenticationError extends Error {
    private errorType: AuthenticationErrorType;
    constructor(type: AuthenticationErrorType, msg?: string) {
        super(msg);
        this.errorType = type;
    }

    getErrorType() {
        return this.errorType;
    }
}

export enum AuthenticationErrorType {
    EMAIL_INVALID,
    USER_DISABLED,
    USER_NOT_FOUND,
    WRONG_PASSWORD,
    NOT_LOGGED_IN,
    UNKNOWN
}