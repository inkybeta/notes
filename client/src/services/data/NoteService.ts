import { Note } from "@/models/text";
import { DirectoryIdentifier } from "..";
import { DataService } from "@/services/data/DataService";
import { PartialUpdate } from "@/models";

export class NoteService extends DataService<Note, PartialUpdate> {
  constructor() {
    super("/api/note", "note_id", {
      type: "note"
    });
  }

  protected deserialize(payload: any): Note {
    return new Note(
      payload["note_id"],
      payload["title"],
      payload["content"],
      new Date(Date.parse(payload["timestamp"])),
      payload["tags"]
    );
  }

  protected serialize(payload: Note): any {
    return {
      note_id: payload.id,
      title: payload.title,
      content: payload.text,
      timestamp: payload.timestamp,
      tags: payload.tags,
      directory_id: payload.parent
    };
  }

  protected serializeUpdate(payload: PartialUpdate) {
    let edits = [];
    for (let i = 0; i < payload.getEdits().length; i++) {
      edits.push({
        action: payload.getEdits()[i].getAction(),
        index: payload.getEdits()[i].getIndex(),
        text: payload.getEdits()[i].getText()
      });
    }
    return {
      note_id: payload.getNoteId(),
      title_update: payload.getTitle(),
      tags: payload.getTags(),
      text_updates: edits,
      timestamp: new Date().toISOString()
    };
  }
}
