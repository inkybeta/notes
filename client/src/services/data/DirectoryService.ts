import { DirectoryIdentifier } from "..";
import Axios from "axios";
import { Directory } from "@/models/Directory";
import { ResourceService } from "@/services/data/DataService";
import { Item, ItemType } from "@/models";
import {
  AuthenticationError,
  AuthenticationErrorType
} from "@/services/errors/AuthenticationError";
import ServerError from "@/services/errors/ServerError";

export class DirectoryService extends ResourceService<Directory> {
  protected serialize(item: Directory) {
    return {
      directory_id: item.id,
      path: item.path,
      name: item.name,
      parent_id: item.parent || null
    };
  }
  protected deserialize(item: any): Directory {

    let children = <Item[]>[];
    for (let i = 0; i < item["children"]["directories"].length; i++) {
      let current = item["children"]["directories"][i];
      let c = new Item(
        ItemType.DIRECTORY,
        current["item_id"],
        current["title"],
        current["parent"]
      );
      children.push(c);
    }

    for (let i = 0; i < item["children"]["notes"].length; i++) {
      let current = item["children"]["notes"][i];
      children.push(
        new Item(
          ItemType.NOTE,
          current["item_id"],
          current["title"],
          current["parent"],
          new Date(Date.parse(current["timestamp"]))
        )
      );
    }

    for (let i = 0; i < item["children"]["articles"].length; i++) {
      let current = item["children"]["articles"][i];
      children.push(
        new Item(
          ItemType.ARTICLE,
          current["item_id"],
          current["title"],
          current["parent"],
          new Date(Date.parse(current["timestamp"]))
        )
      );
    }

    return new Directory(
      item["directory_id"],
      item["path"],
      item["name"],
      item["parent_id"],
      children
    );
  }

  public async read(
    id: string | DirectoryIdentifier | null
  ): Promise<Directory> {
    if (id != null && typeof id === "string") {
      return await super.read(id);
    } else if (id == null) {
      let token = await super.getAuthenticationToken();
      let start = Date.now();
      let result = await Axios.get(this.mapUrl(this.endpoint), {
        params: { directory_id: null },
        headers: {
          Authorization: `Firebase ${token.getToken()}`
        }
      });
      if (result.status == 200) {
        return this.deserialize(result.data.payload);
      } else if (result.status == 401) {
        throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
      } else {
        throw new ServerError();
      }
    } else {
      let query: any = {};
      query[this.idName] = id;
      let result = await Axios.get(this.mapUrl(this.endpoint), {
        params: this.mapData(query),
        headers: {
          Authorization: `Firebase ${(await super.getAuthenticationToken()).getToken()}`
        }
      });
      if (result.status == 200) {
        return this.deserialize(result.data.payload);
      } else if (result.status == 401) {
        throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
      } else {
        throw new ServerError();
      }
    }
  }

  constructor() {
    super("/api/directory", "directory_id");
  }
}
