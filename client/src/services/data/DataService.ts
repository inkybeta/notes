import { AuthenticationToken } from "../authentication/AuthenticationToken";
import { getBaseUrl, DirectoryIdentifier } from "..";
import Axios from "axios";
import ServerError, { ItemMissingError } from "@/services/errors/ServerError";
import { AuthenticationStore } from "@/services/authentication/AuthenticationStore";
import {
  AuthenticationError,
  AuthenticationErrorType
} from "@/services/errors/AuthenticationError";
import { Item } from "@/models/";
import { ItemType } from "@/models/";

/**
 * A service for interacting with a resource on the server. This provides
 * the general outline for a common interface to create, read, update, and
 * delete resources from the service.
 * @argument T the type of service that this implements
 */
export abstract class ResourceService<T> {
  protected authenticationStore!: AuthenticationStore;
  protected endpoint: string;
  protected idName: string;
  protected extra: any | null;

  constructor(endpoint: string, idName: string, extra?: any) {
    this.endpoint = endpoint;
    this.idName = idName;
    this.extra = extra ? extra : null;
  }

  protected async getAuthenticationToken(): Promise<AuthenticationToken> {
    return await this.authenticationStore.getCurrentToken();
  }

  public setAuthentication(authentication: AuthenticationStore) {
    this.authenticationStore = authentication;
  }

  /**
   * Serialize an object into a JSON serializable item. This will likely map fields from
   * camelcase to snakecase.
   * @param item the item to deserialize
   * @returns an object to be sent to the server
   */
  protected abstract serialize(item: T): any;

  protected serializeItem(type: ItemType, item: any): Item {
    if (item["timestamp"]) {
      let date = new Date(Date.parse(item["timestamp"]));
      return new Item(type, item["item_id"], item["title"], item["parent"], date);
    }
    else {
      return new Item(type, item["item_id"], item["title"], item["parent"]);
    }
  }

  /**
   * Deserialize an object from a JSON string into the target object. This will likely map
   * fields from snakecase to camelcase.
   * @param item the item to deserialize.
   * @returns the target object type mapped from the server.
   */
  protected abstract deserialize(item: any): T;

  protected mapUrl(url: string, query: any = null): string {
    if (query) {
      var str = [];
      for (let parameter in query)
        str.push(encodeURIComponent(parameter) + "=" + encodeURIComponent(query[parameter]));
      return getBaseUrl() + url + "?" + str.join("&");
    }
    return getBaseUrl() + url;
  }

  protected mapData(data: any): any {
    return { ...this.extra, ...data };
  }

  /**
   * Creates the resource and returns the id of the item generated.
   * @argument value the object to add to the service
   * @returns an UUID formatted as a hexadecimal string
   * @throws AuthenticationError if the user failed to pass in a valid AuthenticationToken
   */
  public async create(value: T): Promise<string> {
    let result = await Axios.post(
      this.mapUrl(this.endpoint),
      this.mapData(this.serialize(value)),
      {
        headers: {
          Authorization:
            "Firebase " + (await this.getAuthenticationToken()).getToken()
        }
      }
    );
    if (result.status == 200) {
      return result.data.payload;
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }

  /**
   * Fetchs a single item based on its id from the server
   * @argument id the id of the object to fetch from the server
   * @returns a deserialized object from the server
   * @throws AuthenticationError if the user failed to pass in a valid AuthenticationToken
   */
  public async read(id: string): Promise<T> {
    let query = {
      ...this.extra
    };
    query[this.idName] = id;
    let result = await Axios.get(
      this.mapUrl(this.endpoint, query),
      {
        headers: {
          Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
        }
      }
    );
    if (result.status == 200) {
      return this.deserialize(result.data.payload);
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }

  /**
   * Deletes a single object from the server
   * @argument id the id of the object to delete from the server
   * @returns a deserialized object from the server
   * @throws AuthenticationError if the user failed to pass in a valid AuthenticationToken
   */
  public async delete(id: string): Promise<boolean> {
    let query: any = {};
    query[this.idName] = id;
    let result = await Axios.delete(this.mapUrl(this.endpoint), {
      data: this.mapData(query),
      headers: {
        Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
      }
    });
    if (Math.floor(result.status / 100) == 2) {
      return result.data.success;
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }

  public async move(
    id: string,
    directory: string | DirectoryIdentifier
  ): Promise<boolean> {
    let data: any = {};
    data[this.idName] = id;
    if (typeof directory == "string") {
      data["directory_id"] = directory;
    } else if (directory instanceof DirectoryIdentifier) {
      data["directory"] = {
        path: directory.path,
        name: directory.name
      };
    } else {
      return false;
    }
    let result = await Axios.put(
      this.mapUrl(this.endpoint),
      this.mapData(data),
      {
        headers: {
          Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
        }
      }
    );
    if (Math.floor(result.status / 100) == 2) {
      return result.data.success;
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }

  public async directory(
    directory: string | DirectoryIdentifier
  ): Promise<T[]> {
    let data: any = {};
    if (typeof directory == "string") {
      data["directory_id"] = directory;
    } else if (directory instanceof DirectoryIdentifier) {
      data["directory"] = {
        path: directory.path,
        name: directory.name
      };
    } else {
      return [];
    }
    let result = await Axios.get(this.mapUrl(this.endpoint + "/directory"), {
      params: this.mapData(data),
      headers: {
        Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
      }
    });
    if (Math.floor(result.status / 100) == 2) {
      return result.data.success;
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }
}

export abstract class DataService<T, U> extends ResourceService<T> {
  protected abstract serializeUpdate(update: U): any;
  public async update(id: string, update: U): Promise<string> {
    let data: any = {};
    data[this.idName] = id;
    let result = await Axios.patch(this.mapUrl(this.endpoint),
      this.mapData({ ...data, ...this.serializeUpdate(update) }),
      {
        headers: {
          Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
        }
      }
    );
    if (Math.floor(result.status / 100) == 2) {
      return result.data.payload;
    } else if (result.status == 401) {
      throw new AuthenticationError(AuthenticationErrorType.NOT_LOGGED_IN);
    } else {
      throw new ServerError();
    }
  }

  public async path(item_id: string) {
    let data: { [index: string]: string } = {};
    data[this.idName] = item_id;
    let query = this.mapData(data);
    let result = await Axios.get(this.mapUrl(this.endpoint + "/path"), {
      params: query,
      headers: {
        Authorization: `Firebase ${(await this.getAuthenticationToken()).getToken()}`
      }
    });
    let items = [];
    let payload = result.data.payload;
    if (!payload) {
      return null;
    }
    for (let i = 0; i < payload.length; i++) {
      items.push(new Item(ItemType.DIRECTORY, payload[i]["item_id"], payload[i]["title"], payload[i]["parent"]))
    }
    return items;
  }

  constructor(endpoint: string, idName: string, extra?: any) {
    super(endpoint, idName, extra);
  }
}
