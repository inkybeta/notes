import { AuthenticationToken } from "../authentication/AuthenticationToken";
import { DirectoryIdentifier } from "..";
import { ArticleNote } from "@/models";
import { PartialUpdate } from "@/models";
import { DataService } from "@/services/data/DataService";

export class ArticleNoteService extends DataService<
  ArticleNote,
  PartialUpdate
  > {
  protected serializeUpdate(payload: PartialUpdate) {
    let edits = [];
    for (let i = 0; i < payload.getEdits().length; i++) {
      edits.push({
        action: payload.getEdits()[i].getAction(),
        index: payload.getEdits()[i].getIndex(),
        text: payload.getEdits()[i].getText()
      });
    }
    return {
      note_id: payload.getNoteId(),
      title_update: payload.getTitle(),
      tags: payload.getTags(),
      text_updates: payload.getEdits(),
      url: new String(payload.getProperties().get("url")),
      authors: payload.getProperties().get("authors")
    };
  }
  protected serialize(item: ArticleNote) {
    return {
      type: "article_note",
      title: item.title,
      content: item.text,
      tags: item.tags,
      authors: item.authors,
      url: item.url,
      timestamp: item.timestamp,
      published: new Date(),
      directory_id: item.parent
    };
  }
  protected deserialize(payload: any): ArticleNote {
    return new ArticleNote(
      payload["note_id"],
      payload["title"],
      payload["content"],
      payload["url"],
      new Date(Date.parse(payload["timestamp"])),
      payload["directory_id"],
      payload["tags"],
      payload["authors"]
    );
  }
  constructor(authentication?: AuthenticationToken) {
    super("/api/note", "note_id", {
      type: "article_note"
    });
  }
}
