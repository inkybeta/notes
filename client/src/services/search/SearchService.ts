import { AuthenticationStore } from "@/services/authentication/AuthenticationStore";

import { AuthenticationToken } from "@/services/authentication/AuthenticationToken";
import SearchQuery from "@/services/search/SearchQuery";

export default class SearchService {

    protected authenticationStore!: AuthenticationStore;
    constructor() {

    }

    protected async getAuthenticationToken(): Promise<AuthenticationToken> {
        return await this.authenticationStore.getCurrentToken();
    }

    public setAuthentication(authentication: AuthenticationStore) {
        this.authenticationStore = authentication;
    }

    public async search(): Promise<SearchQuery> {
        return new SearchQuery(await this.authenticationStore.getCurrentToken());
    }
}