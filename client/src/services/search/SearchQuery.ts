import { Item, ItemType } from "@/models/Item";
import Axios from "axios";
import { getBaseUrl } from "..";
import { AuthenticationToken } from "services/authentication/AuthenticationToken";

export class SearchResult {
    constructor(public total: number, public items: Item[]) {

    }
}

export default class SearchQuery {
    private mapUrl(url: string) {
        return getBaseUrl() + url;
    }

    constructor(private token: AuthenticationToken) {

    }

    private data: any = {};

    protected deserialize(payload: any): SearchResult {
        let items = [] as Item[];
        for (let i = 0; i < payload.notes.length; i++) {
            items.push(new Item(ItemType.NOTE, payload.notes[i]["item_id"], payload.notes[i]["title"],
                payload.notes[i]["timestamp"],
                new Date(Date.parse(payload.notes[i]["timestamp"]))))
        }
        for (let i = 0; i < payload.articles.length; i++) {
            items.push(new Item(ItemType.DIRECTORY, payload.articles[i]["item_id"], payload.articles[i]["title"],
                payload.notes[i]["parent"],
                new Date(Date.parse(payload.articles[i]["timestamp"]))))
        }
        let directories = payload.directories;
        for (let i = 0; i < payload.articles.length; i++) {
            items.push(new Item(ItemType.DIRECTORY, directories[i]["item_id"], directories[i]["title"],
                payload.notes[i]["parent"],
                new Date(Date.parse(directories[i]["timestamp"]))))
        }
        return new SearchResult(payload["total"], items);
    }

    public title(title: string) {
        this.data["title"] = title;
        return this;
    }

    public content(content: string) {
        this.data["content"] = content;
        return this;
    }

    public tags(tags: string[]) {
        if (!this.data["tags"]) {
            this.data["tags"] = [];
        }
        this.data["tags"].push(...tags);
        return this;
    }

    public start(start: number) {
        this.data["start"] = start;
        return this;
    }

    public limit(limit: number) {
        this.data["count"] = limit;
        return this;
    }

    public url(url: string) {
        this.data["url"] = url;
        return this;
    }

    public async execute(): Promise<SearchResult> {
        let result = await Axios.post(this.mapUrl("/api/search"), this.data, {
            headers: {
                Authorization:
                    "Firebase " + this.token.getToken()
            }
        });
        return this.deserialize(result.data.payload)
    }
}