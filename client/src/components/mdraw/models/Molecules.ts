import { Module } from "vuex";
import RGroup from "./RGroup";
import Bond from "./Bond";
import { niceAngles, standardBondDist } from "../../../constants";

let chord = function(angle: number) {
    return 2 * Math.sin(angle / 2);
  },
  toDegrees = function(radians: number) {
    return (radians * 180) / Math.PI;
  },
  toRadians = function(degrees: number) {
    return (degrees * Math.PI) / 180;
  };

let state = {
  rgroups: [] as RGroup[],
  bonds: [] as Bond[],
  moving: null as RGroup | null,
  creating: false,
  origin: { x: 0, y: 0 }
};

type StateType = { [P in keyof typeof state]: (typeof state)[P] };

let module: Module<StateType, any> = {
  namespaced: true,
  state,
  mutations: {
    addRGroup: function({ rgroups }, rgroup: RGroup) {
      rgroups.push(rgroup);
    },
    addBond: function({ bonds }, bond: Bond) {
      bonds.push(bond);
    },
    moving: function(state, rgroup: RGroup) {
      state.moving = rgroup;
    },
    stopMove: function(state) {
      state.moving = null;
      state.creating = false;
    },
    click: function(state, rgroup: RGroup) {
      if (!state.moving) {
        let next = new RGroup("C", rgroup.x, rgroup.y, 0),
          bond = new Bond(rgroup, next);
        state.rgroups.push(next);
        state.bonds.push(bond);
        state.creating = true;
        state.origin = rgroup;
        state.moving = next;
      }
    },
    move: function(state, { x, y }: { x: number; y: number }) {
      if (state.moving)
        if (state.creating) {
          let angle = Math.atan2(y - state.origin.y, x - state.origin.x);
          let radius = Math.hypot(x - state.origin.x, y - state.origin.y);
          for (let a of niceAngles) {
            //Chord function: crd(theta) = 2 * sin(theta / 2)
            let crd = Math.abs(
              2 * Math.sin(toRadians((a - toDegrees(angle)) / 2))
            );
            if (Math.abs(crd * radius) < 8.75) {
              angle = toRadians(a > 180 ? a - 360 : a);
              break;
            }
          }
          state.moving.x = Math.cos(angle) * standardBondDist + state.origin.x;
          state.moving.y = Math.sin(angle) * standardBondDist + state.origin.y;
        } else {
          state.moving.x = x;
          state.moving.y = y;
        }
    },
    cancel: function(state) {
      if (state.creating) {
        state.bonds.splice(state.bonds.length - 1, 1);
        state.rgroups.splice(state.rgroups.length - 1, 1);
        state.moving = null;
      }
    }
  }
};

export default module;
