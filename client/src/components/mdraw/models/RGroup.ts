let idgen = 0;
export default class {
  id:string = idgen++ + "rgroup";
  constructor(
    public name: string,
    public x: number = 0,
    public y: number = 0,
    public charge: number = 0
  ) {}
}
