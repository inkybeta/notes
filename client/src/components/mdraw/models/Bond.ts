let idgen = 0;

import RGroup from "./RGroup";
export default class {
  id: string = idgen++ + "bond";
  constructor(
    public start: RGroup,
    public end: RGroup,
    public order: number = 1,
    public visualState: VisualState = VisualState.STANDARD
  ) {}
}

enum VisualState {
  STANDARD,
  LEFT,
  RIGHT
}

export { VisualState };
