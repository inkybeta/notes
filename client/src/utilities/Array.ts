function deepCompare<T>(first: T[], second: T[]): boolean {
  if (first.length != second.length) return false;
  for (let i = 0; i < first.length; i++) {
    if (first[i] !== second[i]) return false;
  }
  return true;
}

export class ArraySet<T> extends Array<T> {
  private hashedSet: Map<T, number> = new Map<T, number>();
  add(value: T): this {
    if (this.hashedSet.has(value)) {
      return this;
    }
    this.hashedSet.set(value, this.size);
    super.push(value);
    return this;
  }

  delete(value: T): boolean {
    let index = this.hashedSet.get(value);
    if (!index) {
      return false;
    }
    super.splice(index, 1);
    return true;
  }

  has(value: T): boolean {
    return this.hashedSet.has(value);
  }

  get size(): number {
    return super.length;
  }

  [Symbol.toStringTag]: "ArraySet";
}

export { deepCompare };

let removeFromArray = function<E>(array: E[], element: E) {
  for (let i = 0; i < array.length; i++) {
    if (element === array[i]) {
      array.splice(i, 1);
      break;
    }
  }
};

export { removeFromArray };
