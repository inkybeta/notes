export class LatexMatch implements IteratorResult<string> {
    public done!: boolean;
    public value!: string;
    constructor(done: boolean) {
        this.done = done;
    }
}

class StringMachine {
    private index: number = 0;
    constructor(private str: string) { }

    reset() {
        this.index = 0;
    }

    match(character: string): boolean {
        if (this.str[this.index] == character) {
            this.index++;
        } else {
            this.reset();
        }
        if (this.index == this.str.length) {
            this.index = 0;
            return true;
        }
        return false;
    }
}

export class LatexMatcher implements Iterator<string> {
    private lastRead!: number;
    private text!: string;
    private machines: StringMachine[];

    // This marks if the previous character was a special
    // character. -1 If it was not, 1 if it was a $ and 2
    // if it was a /.
    private previousCharState: number = -1;

    constructor(escape: string[]) {
        this.lastRead = 0;
        this.text = "";
        this.machines = [] as StringMachine[];
        for (let i in escape) {
            this.machines.push(new StringMachine(i))
        }
    }

    read(text: string) {
        this.text = text;
    }

    reset() {
        this.lastRead = 0;
    }

    next(): IteratorResult<string> {
        let begin = -1;
        let escaped = false;
        let matcher = null as StringMachine | null;
        while (this.lastRead++ < this.text.length) {
            for (let i = 0; i < this.machines.length; i++) {
                let machine = this.machines[i];
                let match = machine.match(this.text[this.lastRead]);
                if (matcher == null && match) {
                    matcher = machine;
                    begin = this.lastRead;
                } else if (machine != null && match && machine == matcher) {
                    let match = new LatexMatch(false);
                    match.value = this.text.substring(begin, this.lastRead + 1);
                    return match;
                }
            }
            if (escaped) {
                escaped = false;
                continue;
            } else if (this.text[this.lastRead] == "\\") {
                escaped = true;
            }
        }
        return new LatexMatch(false);
    }
}