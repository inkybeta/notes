export interface Processor {
    process(text: string): string;
}

export interface Preprocessor extends Processor {

}

export interface Postprocessor extends Processor {
    
}