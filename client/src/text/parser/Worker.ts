export interface Worker {
    render(p: string): Promise<string>;
}

export class SynchronousLatexWorker implements Worker {
    constructor(private katex: any, private displayMode: any) {
    }

    async render(p: string): Promise<string> {
        return this.katex.renderToString(p, this.displayMode);
    }
}