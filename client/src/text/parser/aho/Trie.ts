export class Trie<T> {
    // There are no nodes that will allow you to continue. We can move to a failure state and attempt
    // to recuperate from possible success states.
    failure!: Trie<T>;
    children!: Map<string, Trie<T>>;

    beginning!: T;
    end!: T;

    constructor() {
        this.children = new Map<string, Trie<T>>();
    }

    private pushCharacter(word: string, index: number, obj: T, beginning: boolean) {
        if (index >= word.length - 1) {
            if (beginning)
                this.beginning = obj;
            else
                this.end = obj;
            return;
        }
        if (!this.children.has(word[index])) {
            this.children.set(word[index], new Trie());
        }
        this.children.get(word[index])!.pushCharacter(word, index + 1, obj, beginning);
    }

    add(word: string, item: T, beginning: boolean) {
        // -1 is the root
        this.pushCharacter(word, 0, item, beginning);
    }
}