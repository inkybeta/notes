export class CorasickMatch {
    public index!: number;
    public group!: number;
    public hash!: number;
    public text!: string;

    constructor(text: string, index: number, group: number, hash: number = -1) {
        this.text = text;
        this.index = index;
        this.group = group;
        this.hash = hash;
    }
}
