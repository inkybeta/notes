import { Delimiter } from "./Delimiter";
import { Trie } from "./Trie";
import { CorasickMatch } from "./Match";

export class AhoCorasick {
    private escape: boolean = false;
    private readonly tree: Trie<Delimiter> = new Trie();

    construtor(matches: Delimiter[]) {
        this.build(matches);
    }

    private build(matches: Delimiter[]) {
        for (let i = 0; i < matches.length; i++) {
            this.tree.add(matches[i].first, matches[i], true);
            this.tree.add(matches[i].second, matches[i], false);
        }

        // Build the failure states for both the beginning and the 
        // ends of each Trie node.
        let q = [] as Trie<Delimiter>[]
        q.push(this.tree);
        while (q.length > 0) {
            let first = q.shift()!;

            first.children.forEach((value, key) => {
                q.push(value);
                let failure = first.failure ? first.failure : this.tree;
                while (failure != this.tree && !failure.children.has(key)) {
                    failure = failure.failure ? failure.failure : this.tree;
                }
                if (failure.children.has(key))
                    value.failure = failure.children.get(key)!;
            })
        }
    }

    private state: Trie<Delimiter> = this.tree;

    private checkChildren(text: string): boolean {
        if (this.state.children.has(text)) {
            this.state = this.state.children.get(text)!;
            return true;
        }
        return false;
    }

    private checkFailure(text: string): boolean {
        if (this.state.failure) {
            this.state = this.state.failure;
            return this.checkChildren(text);
        }
        return false;
    }

    /**
     * Matches the character given by text to a delimiter if a match occurred.
     * Otherwise, returns null.
     * @param text the character to match
     */
    private matchBeginning(text: string): Delimiter | null {
        // We have a child so we do not have to rely on failure states to get us
        // through
        if (this.checkChildren(text)) {
            return this.state.beginning ? this.state.beginning : null;
        }

        // Try to find the failure state that matches
        if (this.checkFailure(text)) {
            return this.state.beginning ? this.state.beginning : null;
        }

        // There isn't a failure state to cover so we need to return back to the root
        this.state = this.tree;
        return null;
    }

    /**
     * 
     * @param text 
     * @param delimiter 
     */
    private matchEnd(text: string, delimiter: Delimiter): boolean {
        // We have a child so we do not have to rely on failure states to get us
        // through
        if (this.checkChildren(text)) {
            return this.state.end == delimiter;
        }

        // Try to find the failure state that matches
        if (this.checkFailure(text)) {
            return this.state.end == delimiter;
        }

        // There isn't a failure state to cover so we need to return back to the root
        this.state = this.tree;
        return false;
    }

    /**
     * Reads a body of text and segments it into various CorasickMatches
     * that associate each text segment with a delimiter.
     * @param text the text to be read
     * @returns a list of CorasickMatch segments that represent segments of
     * a string and their respective Delimiters
     */
    public read(text: string) {
        for (let i = 0; i < text.length; i++) {
            this.matchBeginning(text[i]);
        }
        return [];
    }
}