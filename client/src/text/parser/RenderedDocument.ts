import { CorasickMatch } from "./aho/Match";

export class RenderedDocument {
    private segments!: CorasickMatch[];
    constructor() {

    }

    /**
     * Combine the segments of the document into a single string
     * to be displayed as an HTML element.
     * @argument soft Render even if there are errors in the rendering
     *                process.
     */
    render(soft: boolean): string {
        if (soft)
            return this.segments.join();
        let rendered: string = "";
        for (let segment in this.segments) {
            rendered += segment;
        }
        return rendered;
    }
}