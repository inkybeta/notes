/**
 * Item is a brief representation of an object in the tree.
 * @param T the id type to use
 */
export class Item {
  public readonly type: ItemType;
  private _id: string;
  private _title: string;
  private _timestamp: Date;
  private _parent: string;

  constructor(type: ItemType, id: string, title: string, parent: string, timestamp: Date = new Date()) {
    this.type = type;
    this._id = id;
    this._title = title;
    this._timestamp = timestamp;
    this._parent = parent || "";
  }

  set id(id: string) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set title(title: string) {
    if (this._title !== title) {
      this._title = title;
    }
  }

  get title() {
    return this._title;
  }

  set timestamp(timestamp: Date) {
    timestamp = timestamp || new Date();
    if (
      this._timestamp !== timestamp &&
      this._timestamp.getTime() !== timestamp.getTime()
    ) {
      this._timestamp = timestamp;
    }
  }

  get timestamp() {
    return this._timestamp;
  }

  get parent() {
    return this._parent;
  }

  set parent(parent: string) {
    this._parent = parent;
  }
}

export enum ItemType {
  DIRECTORY = 0,
  NOTE = 1,
  ARTICLE = 2,
  FILE = 3
}
