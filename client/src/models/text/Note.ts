import { DirectoryIdentifier } from "@/services";
import { Item, ItemType } from "@/models/Item";

export class Note extends Item {
  private _text: string;
  private _tags: Set<string>;
  public directory!: DirectoryIdentifier | null;

  constructor(id: string, title: string, text: string, timestamp: Date, directoryId: string, tags?: string[],
    directory?: DirectoryIdentifier) {
    super(ItemType.NOTE, id, title, directoryId, timestamp);
    this._text = text;
    this.directory = directory ? directory : null;
    this._tags = new Set<string>(tags);
  }

  get text(): string {
    return this._text;
  }

  set text(text: string) {
    this._text = text;
  }

  public addTag(tag: string) {
    this._tags.add(tag);
  }

  public removeTag(tag: string) {
    this._tags.delete(tag);
  }

  get tags(): string[] {
    return Array.from(this._tags.values())
  }
}