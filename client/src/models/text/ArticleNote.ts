import { Note } from './Note';
import { DirectoryIdentifier } from '@/services';

/**
 * ArticleNote represents an article that the user has linked.
 */
export class ArticleNote extends Note {
  private _url: string;
  private _authors: Set<string>;

  constructor(uuid: string, title: string, text: string, url: string,
    timestamp: Date, directoryId: string, tags?: string[], authors?: string[]) {
    super(uuid, title, text, timestamp, directoryId, tags)
    this._url = url
    this._authors = new Set(authors)
  }

  get url(): string {
    return this._url;
  }

  get authors(): string[] {
    return Array.from(this._authors.values());
  }

  public addAuthor(author: string) {
    this._authors.add(author);
  }

  public removeAuthor(author: string) {
    this._authors.delete(author);
  }
}