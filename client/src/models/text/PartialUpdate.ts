export class PartialUpdate {
    private _noteId: string;
    private _title: string | null;
    private _tags: Set<string> | null;
    private _edits: Edit[];
    private _properties: Map<string, any> | null;

    constructor(noteId: string, edits?: Edit[], title?: string, tags?: Set<string>,
        properties?: Map<string, any>) {
        this._noteId = noteId;
        if (edits)
            this._edits = edits;
        else
            this._edits = [];
        if (title)
            this._title = title
        else
            this._title = null;
        if (tags)
            this._tags = new Set<string>(tags);
        else
            this._tags = null;
        this._properties = properties ? properties : null;
    }

    getNoteId(): string {
        return this._noteId;
    }

    getTitle() {
        return this._title;
    }

    getTags() {
        return this._tags ? Array.from(this._tags.values()) : <string[]>[];
    }

    getEdits() {
        return this._edits ? Array.from(this._edits.values()) : <Edit[]>[];
    }

    getProperties(): Map<string, any> {
        return this._properties ? this._properties : new Map<string, any>();
    }

    apply(text: string) {
        this._edits.forEach(e => {
            let action = e.getAction();
            if (action == EditAction.DELETION) {
                text = text.substring(0, e.getIndex()) + text.substring(e.getIndex() + e.getText().length);
            } else if (action == EditAction.INSERTION) {
                text = text.substring(0, e.getIndex()) + e.getText() + text.substring(e.getIndex());
            } else {
                text = text.substring(0, e.getIndex()) + e.getText() + text.substring(e.getIndex() + e.getText().length);
            }
        });
        return text;
    }

    merge(former: PartialUpdate) {
        let sequence = former.getEdits();
        sequence.sort((a, b) => a.getIndex() - b.getIndex())

        function minimum(a: Edit, b: Edit) {
            return a.getIndex() < b.getIndex() ? a : b;
        }

        // We attempt to naively accumulate updates with this method
        let shiftedDocumentLength = 0;
        let edits = [];
        for (let i = 0; i < sequence.length + this._edits.length; i++) {
            
        }
        return new PartialUpdate(this._noteId, this._edits, this._title ? this._title : undefined,
            this._tags ? this._tags : undefined, this._properties ? this._properties : undefined);
    }
}

export class Edit {
    private _action: EditAction;
    private _index: number;
    private _text: string;

    constructor(action: EditAction, index: number, text: string) {
        this._action = action;
        this._index = index;
        this._text = text;
    }

    getAction() {
        return this._action;
    }

    getIndex() {
        return this._index;
    }

    getText() {
        return this._text;
    }
}

export enum EditAction {
    INSERTION = 0,
    DELETION = 1,
    REPLACE = 2
}