import { Item, ItemType } from "./Item";
import { DirectoryIdentifier } from "@/services";

type DirectoryLabel = string;

export { DirectoryLabel };

export class Directory extends Item {
  public path!: string;
  public name!: string;

  constructor(
    uuid: string,
    path: string,
    name: string,
    parentId: string,
    public children: Item[] = []
  ) {
    super(ItemType.DIRECTORY, uuid, name, parentId);
    this.path = path;
    this.name = name;
  }
}

export class SimpleDirectory {
  private _id: string;
  private _name: string;
  private _path: string;

  constructor(id: string, name: string, path: string) {
    this._id = id;
    this._name = name;
    this._path = path;
  }

  getId() {
    return this._id;
  }

  getName() {
    return this._name;
  }

  getPath() {
    return this._path;
  }
}
