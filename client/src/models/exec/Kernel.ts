export class Kernel {
    constructor(code: string) {

    }
}

export class KernelResult {
    
}

export class KernelState {
    private _state: KernelStateType;

    constructor(state: KernelStateType) {
        this._state = state;
    }
}

export enum KernelStateType {
    SUSPENDED = 0,
    RUNNING = 1,
    FINISHED = 2
}