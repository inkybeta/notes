import { Item, ItemType } from "./Item";
import { PartialUpdate, Edit, EditAction, ArticleNote, Note } from "./text";
import { Directory } from "./Directory";
export {
  Note,
  PartialUpdate,
  Directory,
  ArticleNote,
  Item,
  ItemType,
  getImageSource,
  getName,
  EditAction,
  Edit
};

let getImageSource = function (type: ItemType, ...args: any[]) {
  let base: string = "/static/assets/fontawesome/";
  switch (type) {
    case ItemType.DIRECTORY:
      if (args[0]) {
        return base + "folder-open-regular.svg";
      } else {
        return base + "folder-regular.svg";
      }
    case ItemType.NOTE:
      return base + "file-alt-solid.svg";
    case ItemType.ARTICLE:
      return base + "link-solid.svg";
    default:
      return "/";
  }
};

let getName = function (type: ItemType) {
  switch (type) {
    case ItemType.DIRECTORY:
      return "Directory"
    case ItemType.NOTE:
      return "Note"
    case ItemType.ARTICLE:
      return "Article"
  }
}