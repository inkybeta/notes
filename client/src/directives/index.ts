import Vue, { VNodeDirective, VNode } from "vue";

const exteriorClick = Vue.directive("exterior-click", {
    bind(el: any, binding: VNodeDirective, node: VNode) {
        el.exteriorClick = (ev: MouseEvent) => {
            if (!(el == ev.target || el.contains(ev.target))) {
                let expression = <string>binding.expression;
                if (node.context)
                    (<Function>(<any>node.context)[expression])(event, el);
            }
        };
        document.body.addEventListener("click", el.exteriorClick);
    }
});

export { exteriorClick }