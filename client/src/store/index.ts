import Vuex from "vuex";
import Vue from "vue";
import AuthenticationModule from "./service/Authentication";
import ServiceModule from "./service";
import ArtifactModule from "./artifacts";
// import MoleculeModule from "../components/mdraw/models/Molecules";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: { theme: "standard" },
  mutations: {
    theme: function(state, theme: string) {
      state.theme = theme;
    }
  },
  modules: {
    auth: AuthenticationModule,
    services: ServiceModule,
    // molecules: MoleculeModule,
    artifacts: ArtifactModule
  }
});

export default store;
