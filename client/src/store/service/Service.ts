import { Module } from "vuex";
import { AuthenticationStore } from "@/services/authentication/AuthenticationStore";
import { NoteService, ArticleNoteService, DirectoryService } from "@/services/data";
import AuthenticationModule from "./Authentication";
import SearchService from "@/services/search/SearchService";

let state = {
    authentication: new AuthenticationStore(),
    note: new NoteService(),
    article: new ArticleNoteService(),
    directory: new DirectoryService(),
    search: new SearchService()
};

type StateType = { [P in keyof typeof state]: (typeof state)[P] };

let module: Module<StateType, any> = {
    namespaced: true,
    state: state,
    modules: {
        auth: AuthenticationModule
    },
    mutations: {
        configureAuthentication(state, config) {
            state.authentication.configure(config);
            state.note.setAuthentication(state.authentication);
            state.article.setAuthentication(state.authentication);
            state.directory.setAuthentication(state.authentication);
            state.search.setAuthentication(state.authentication);
        }
    },
    actions: {
        configureAuthentication({ state, commit }, config) {
            commit("configureAuthentication", config);
        }
    },
    getters: {
        note(state) {
            return state.note;
        },
        auth(state) {
            return state.authentication;
        },
        article(state) {
            return state.article;
        },
        directory(state) {
            return state.directory;
        },
        search(state) {
            return state.search;
        }
    }
};

export default module;
