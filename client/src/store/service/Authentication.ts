import { Module } from "vuex";

import {
  AuthenticationError,
  AuthenticationErrorType
} from "@/services/errors/AuthenticationError";
import {
  AuthenticationToken,
  AuthenticationType
} from "@/services/authentication/AuthenticationToken";
import User from "@/models/auth/User";
import { AuthenticationStore } from "@/services/authentication/AuthenticationStore";

let state = {
  user: new User(),
  authenticationToken: new AuthenticationToken()
};

type StateType = { [P in keyof typeof state]: (typeof state)[P] };

const module: Module<StateType, any> = {
  namespaced: true,
  state,
  mutations: {
    setUser(state, user: User) {
      state.user = user;
    },
    setToken(state, token: AuthenticationToken) {
      state.authenticationToken = token;
    }
  },
  actions: {
    async startup({ rootGetters, commit }) {
      let authentication: AuthenticationStore = rootGetters["services/auth"];
      try {
        let token = await authentication.getCurrentToken();
        commit("setToken", token);
        let user = await authentication.getUser(token);
        commit("setUser", user);
        let last = localStorage.getItem("lastRefresh");
        if (!last) {
          localStorage.setItem("lastRefresh", Date.now().toString());
          authentication.refreshToken().then(f => commit("setToken", f));
        } else {
          let lastTime = Date.parse(last);
          if (lastTime - Date.now() > 900000) {
            localStorage.setItem("lastRefresh", Date.now().toString());
            authentication.refreshToken().then(f => commit("setToken", f));
          }
        }
        return token;
      } catch (f) {
        if (f instanceof AuthenticationError) {
          if (f.getErrorType() == AuthenticationErrorType.NOT_LOGGED_IN) {
            commit("setToken", new AuthenticationToken());
          }
        } else {
          throw f;
        }
      }
    },
    loginWithEmailAndPassword(
      { state, commit, rootGetters },
      { email, password }
    ) {
      let authentication: AuthenticationStore = rootGetters["services/auth"];
      return authentication
        .loginWithEmailAndPassword(email, password)
        .then(function(token: string | null) {
          commit(
            "setToken",
            new AuthenticationToken(AuthenticationType.FIREBASE, token)
          );
          return authentication
            .getUser(state.authenticationToken)
            .then(user => commit("setUser", user));
        })
        .then(() => true);
    },
    logout({ state, commit, rootGetters }) {
      let authentication: AuthenticationStore = rootGetters["services/auth"];
      return authentication.logout(state.authenticationToken).then(() => {
        commit("setToken", new AuthenticationToken());
        return true;
      });
    },
    register({ rootGetters }, { user, password }) {
      let authentication: AuthenticationStore = rootGetters["services/auth"];
      return authentication
        .register(user, password)
        .then(function(success: boolean) {
          return success;
        });
    },
    getLoggedIn({ rootGetters }) {
      let authentication: AuthenticationStore = rootGetters["services/auth"];
      return authentication.getCurrentToken().then(f => {
        return f.isLoggedIn();
      });
    }
  },
  getters: {
    token(state) {
      return state.authenticationToken.token;
    },
    getFirstName(state) {
      return state.user.firstName;
    },
    user(state) {
      return state.user;
    }
  }
};

export default module;
