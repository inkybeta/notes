import { ActionContext } from "vuex";

import { StateType } from "../state";

import { ItemType } from "../../../models";

const deleteActions = {
  delete: async function(store: ActionContext<StateType, any>, id: string) {
    if (store.state.pool.has(id)) {
      let type = await store.dispatch("deleteImpl", id);
      store.dispatch("deleteFromServer", { id, type });
    }
  },
  deleteImpl: function(
    store: ActionContext<StateType, any>,
    id: string
  ): ItemType {
    let type: ItemType = store.state.pool.get(id)!.item.get().type;
    let childrenToAlsoDelete = store.state.pool.get(id)!.children!;
    store.commit("deleteParentRefs", id);
    if (store.state.openNotes.includes(id)) {
      store.commit("close", id);
    }
    if (store.state.focused === id) {
      store.state.focused = "";
    }
    store.commit("clearFromPool", id);
    childrenToAlsoDelete.forEach((s: string) =>
      store.dispatch("artifacts/delete", { id: s, type })
    );
    return type;
  },
  deleteFromServer: async function(
    store: ActionContext<StateType, any>,
    { id, type }: { id: string; type: number }
  ) {
    return type !== undefined && type !== null && type !== ItemType.FILE
      ? await store.rootGetters[
          "services/" +
            (type == 0 ? "directory" : type == 1 ? "note" : "article")
        ].delete(id)
      : null;
  }
};

export { deleteActions };
