import { ActionContext } from "vuex";
import { StateType } from "../state";
import { Item, ItemType, Note, ArticleNote, PartialUpdate } from "@/models";
import { NoteService, ArticleNoteService } from "@/services/data";
import { refreshTime } from "@/constants";
import { unsavedIdRoot, ArtifactActionContext } from "./constants";
import { createActions } from "./create";
import { deleteActions } from "./delete";
import { searchActions } from "./search";
import { sharedActions } from "./shared";
import state from "../state";

export class LoadingError extends Error {
  constructor() {
    super("There was an error loading the requested item");
  }
}

let actions = {
  /**
   * Update the children associated with the action.
   * @param store The store state of the artifacts object. This is passed by Vuex after
   *              calling dispatch() on the store
   * @param param An object containing the ID of the directory and the updated children.
   */
  setChildren(
    store: ArtifactActionContext,
    { id, children }: { id: string; children: Item[] }
  ) {
    let ids = children.map(i => i.id);
    children.filter(i => !store.state.pool.has(i.id)).forEach(i => {
      store.commit("add", i);
    });
    children.forEach(i => store.commit("update", { item: i, full: false }));
    store.state.pool.get(id)!.children = ids;
  },
  async open(store: ActionContext<StateType, any>, id: string) {
    if (store.state.pool.has(id) && !store.state.pool.get(id)!.opening) {
      if (store.state.openNotes.includes(id)) {
        store.commit("select", id);
      } else {
        let item = store.state.pool.get(id)!.item.get();
        let service: NoteService | ArticleNoteService =
            store.rootGetters[
              "services/" + (item.type === ItemType.NOTE ? "note" : "article")
            ],
          note: Note | ArticleNote = await (store.state.pool.get(
            id
          )!.opening = service.read(id));
        store.commit("update", { item: note, full: true });
        store.commit("save", id);
        store.commit("open", id);
        store.commit("select", id);
        store.state.pool.get(id)!.opening = null;
      }
    } else if (store.state.pool.has(id)) {
      await store.state.pool.get(id)!.opening;
    }
  },
  save: async function(store: ActionContext<StateType, any>, id: string) {
    if (store.state.pool.has(id)) {
      let item = store.state.pool.get(id)!.item.get();
      if (!(item instanceof Note)) {
        throw new LoadingError();
      }
      if (id.includes(unsavedIdRoot)) {
        store.commit("queueSave", id);
      } else {
        let updates = store.state.pool.get(id)!.updateQueue.slice(0);
        let condensed = new PartialUpdate(
          id,
          updates.flatMap(p => p.getEdits()),
          item.title,
          new Set<string>(item.tags)
        );
        store.commit("save", id);
        await store.rootGetters[
          "services/" + (item instanceof ArticleNote ? "article" : "note")
        ].update(id, condensed);
      }
    }
  },
  /**
   * Get the children of a directory.
   * @param store
   * @param id
   */
  async getChildren(store: ArtifactActionContext, id: string): Promise<Item[]> {
    // There are no children of unsaved items yet. Return an empty array
    if (id && id.startsWith(unsavedIdRoot)) return [];
    let cacheItem = store.state.pool.get(id)!;
    let cached: string[] = cacheItem.children;
    let expired = cached.filter(m => {
      store.state.pool.get(m)!.item.expiredHeader(refreshTime);
    }).length;
    let timeElapsed = Date.now() - cacheItem.lastLoadTime.getTime();
    if ((timeElapsed > refreshTime && !cached.length) || expired) {
      // There's already a request pending; cancel this one
      if (cacheItem.loading || cacheItem.opening) {
        await (cacheItem.loading || cacheItem.opening);
        return cached.map(s => store.state.pool.get(s)!.item.get());
      }
      let children = (await (store.state.pool.get(
        id
      )!.loading = store.rootGetters["services/directory"].read(id))).children;
      children.forEach((c: Item) => (c.parent = id));
      await store.dispatch("setChildren", { id, children });
      //We're done loading stuff; clear the relevant flags
      cacheItem.loading = null;
      cacheItem.lastLoadTime = new Date();
      return children;
    } else {
      return cached.map(s => store.state.pool.get(s)!.item.get());
    }
  },
  /**
   * Replace an unsaved id with the ID assigned by the server. This ensures that notes
   * can be edited before they are fully created on the server.
   * @param store The state of the store. This should be passed in by Vuex.
   * @param param1 Two parameters containing the ID of the unsaved target and its replacement.
   */
  async replace(
    store: ActionContext<StateType, any>,
    { target, replacement }: { target: string; replacement: string }
  ) {
    if (store.state.pool.has(target)) {
      store.commit("replace", { target, replacement });
      if (store.state.pool.get(replacement)!.unsavedChanges) {
        await store.dispatch("save", replacement);
      }
    }
  },
  closeAllTabs: function(store: ArtifactActionContext) {
    for (let i = store.state.openNotes.length - 1; i --> -1; ) {
      store.commit("close", store.state.openNotes[i]);
    }
    store.state.openNotes.forEach(n => store.commit("close", n));
  },
  async refresh(store: ActionContext<StateType, any>, id: string) {},
  ...createActions,
  ...deleteActions,
  ...searchActions,
  ...sharedActions
};

export default actions;
