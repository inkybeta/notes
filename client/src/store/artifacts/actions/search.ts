import { ActionContext } from "vuex";

import { StateType } from "../state";

import { ItemType, Item } from "../../../models";
import SearchService from "@/services/search/SearchService";
import { ArticleNoteService, NoteService } from "@/services/data";

const searchActions = {
  async search(
    ctx: ActionContext<StateType, any>,
    {
      text
    }: {
      text: string;
    }
  ) {
    let search: SearchService = ctx.rootGetters["services/search"];
    let result = await (await search.search()).content(text).execute();
    return result;
  },

  async getPath(
    ctx: ActionContext<StateType, any>,
    { id, type }: { id: string; type: ItemType }
  ) {
    if (type == ItemType.NOTE) {
      let noteService: NoteService = ctx.rootGetters["services/note"];
      return noteService.path(id);
    } else if (type == ItemType.ARTICLE) {
      let articleService: ArticleNoteService =
        ctx.rootGetters["services/article"];
      return articleService.path(id);
    } else {
      return [];
    }
  },

  loadPath: async function(
    store: ActionContext<StateType, any>,
    { id, type }: { id: string; type: ItemType }
  ) {
    let items: string[] = (await store.dispatch("getPath", { id, type })).map(
      (item: Item) => item.id
    );
    items.splice(0, 0, "");
    for (let i = 0; i < items.length; i++) {
      await store.dispatch("getChildren", items[i]);
    }
  }
};

export { searchActions };
