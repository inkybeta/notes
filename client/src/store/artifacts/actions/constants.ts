import { StateType } from "../state";
import { ActionContext } from "vuex";

let nextId = 0;
const unsavedIdRoot = "Unsaved",
  getAndIncrement = function() {
    return nextId++;
  },
  startText =
    "<!--Styling-->\n\
<style>\n\
  h1, h2, h3, h4, .center {\n\
    text-align: center;\n\
  }\n\
  .small-text {\n\
    font-size: 0.85em;\n\
  }\n\
  td, th {\n\
    border-style: solid;\n\
    border-color: black;\n\
    border-width: 1px;\n\
  }\n\
  em {\n\
    font-weight: bold;\n\
  }\n\
</style>",
  getPath = function(
    store: ArtifactActionContext,
    parentId: string | null
  ): string {
    if (!parentId) {
      return "/";
    }
    let path: string[] = [
        store.state.pool.get(parentId)!.item.get().title + "/"
      ],
      upper: string | null = parentId;
    while ((upper = store.state.pool.get(upper)!.item.get().parent)) {
      path.push(store.state.pool.get(upper)!.item.get().title);
    }
    path.push("");
    return path.reverse().join("/");
  };
type ArtifactActionContext = ActionContext<StateType, any>;

export {
  unsavedIdRoot,
  getAndIncrement,
  ArtifactActionContext,
  startText,
  getPath
};
