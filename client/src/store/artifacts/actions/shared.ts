import { ArtifactActionContext } from "./constants";
import { Item, ItemType } from "@/models/";

const sharedActions = {
    getService(ctx: ArtifactActionContext, item: Item) {
        switch (item.type) {
            case ItemType.NOTE:
                return ctx.rootGetters["services/note"];
            case ItemType.ARTICLE:
                return ctx.rootGetters["services/article"];
            case ItemType.DIRECTORY:
                return ctx.rootGetters["services/directory"];
            default:
                throw new Error("What kind of type was that?")
        }
    }
}

export { sharedActions }