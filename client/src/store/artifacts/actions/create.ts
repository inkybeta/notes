import { ActionContext } from "vuex";

import { StateType } from "../state";

import { Note, Directory, Item, ArticleNote } from "@/models";

import {
  unsavedIdRoot,
  getAndIncrement,
  ArtifactActionContext,
  startText,
  getPath
} from "./constants";

const createActions = {
  async create(store: ArtifactActionContext, item: Item) {
    item.id = unsavedIdRoot + getAndIncrement();
    store.commit("add", item);
    await store.dispatch("createOnServer", item);
    await store.dispatch("open", item.id);
  },
  createNote: async function (store: ArtifactActionContext, { name, parentId }: { name: string; parentId: string }
  ) {
    await store.dispatch(
      "create",
      new Note("", name, startText, new Date(), parentId)
    );
  },
  createArticleNote: async function (
    store: ArtifactActionContext,
    {
      name,
      url,
      parentId
    }: { name: string; url: string; parentId: string }
  ) {
    await store.dispatch(
      "create",
      new ArticleNote("", name, startText, url, new Date(), parentId)
    );
  },
  async createDirectory(
    store: ArtifactActionContext,
    { name, parentId }: { name: string; parentId: string }
  ) {
    await store.dispatch(
      "create",
      new Directory("", getPath(store, parentId), name, parentId)
    );
  },
  createOnServer: async function (
    store: ActionContext<StateType, any>,
    item: Note | Directory
  ) {
    await store.dispatch("replace", {
      target: item.id,
      replacement: await store.rootGetters[
        "services/" +
        (item instanceof Directory
          ? "directory"
          : item instanceof ArticleNote
            ? "article"
            : "note")
      ].create(item)
    });
  }
};

export { createActions };
