import state from "./state";
import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import { StateType } from "./state";
import { Module } from "vuex";

let module: Module<StateType, any> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};

export default module;
