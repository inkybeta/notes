import { Item, PartialUpdate } from "../../models";
import { ItemType } from '../../models/Item';

/**
 * ServerIdentifaction for notes is a UUID which is represented as a hex string.
 * This cannot be null unlike directory identifiers.
 */
type ServerIdentification = string;

/**
 * A class that represents an item that has a marked expiration date.
 * Every time the item is set via the setter or via set() the expiration
 * time is updated.
 */
export class ExpiringItem<T> {
  /**
   * The time at which this object was last set or updated
   */
  public headerExpiry!: Date;
  public contentExpiry!: Date;

  constructor(private item: T) {
    this.headerExpiry = new Date();
    this.contentExpiry = new Date();
  }

  get() {
    return this.item;
  }

  set(val: T, full: boolean) {
    this.item = val;
    this.headerExpiry = new Date();
    if (full) {
      this.contentExpiry = new Date();
    }
  }

  /**
   * A method to determine whether an object is expired or not.
   * @param max The maximum number of milliseconds that the cache is valid for
   */
  expiredHeader(max: number): boolean {
    return Date.now() - this.headerExpiry.getTime() > max;
  }

  expiredContent(max: number): boolean {
    return Date.now() - this.contentExpiry.getTime() > max;
  }
}

/**
 * A CachedItem that represents an expiring Item and the expiring Object it
 * represents. It also stores information about the relationship of this Item
 * to other items in the Tree structure
 */
export class CacheItem {
  public item!: ExpiringItem<Item>;
  public updateQueue: PartialUpdate[];
  public unsavedChanges!: boolean;
  public loading!: Promise<any> | null;
  public lastLoadTime!: Date;
  public opening!: Promise<any> | null;
  public children!: string[];

  constructor(item: Item, unsavedChanges: boolean) {
    this.item = new ExpiringItem(item);
    this.unsavedChanges = unsavedChanges;
    this.loading = null;
    this.opening = null;
    this.lastLoadTime = new Date(0);
    this.updateQueue = [];
    this.children = [];
  }
}

let state = {
  //This is a giant pool of data
  pool: new Map<string, CacheItem>([
    //Shut up idc im never going to call the fake item at the root
    ["", new CacheItem(new Item(ItemType.FILE, "", "", ""), false)]
  ]),
  /**
   * A list of IDs that are currently open in the tabs.
   */
  openNotes: [] as ServerIdentification[],
  focused: "" as ServerIdentification,
  selected: "" as ServerIdentification,
  currentHTML: ""
};

export default state;

type StateType = { [P in keyof typeof state]: (typeof state)[P] };
export { StateType };
