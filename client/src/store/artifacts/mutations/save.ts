import { StateType } from "../state";
import { PartialUpdate } from "../../../models";

const saveActions = {
  /**
   * Ensure that a cached item is marked to be saved upon creation.
   * @param id the id of the Note to be marked with unsaved changes.
   */
  queueSave(state: StateType, id: string) {
    state.pool.get(id)!.unsavedChanges = true;
  },
  /**
   * Clear the update queue
   * @param state
   * @param id
   */
  save(state: StateType, id: string) {
    let updateQueue = state.pool.get(id)!.updateQueue;
    updateQueue.splice(0, updateQueue.length);
  },
  queueUpdate(
    state: StateType,
    { id, update }: { id: string; update: PartialUpdate }
  ) {
    if (state.pool.has(id)) {
      state.pool.get(id)!.updateQueue.push(update);
    }
  }
};

export { saveActions };
