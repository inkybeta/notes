import { StateType, CacheItem } from "../state";
import { Item, ItemType } from "@/models";
import { removeFromArray } from "@/utilities/Array";
import clearActions from "./clear";
import { saveActions } from "./save";

let saveTabs = function(state: StateType, tabs: string[]) {
  return tabs.map(t => [t, state.pool.get(t)!.item.get().type]);
};

let mutations = {
  /**
   * Update a cached item with the specified item if it exists in the pool
   * @param state The state of the tree
   * @param item The updated item. It should have the same ID.
   */
  update(state: StateType, { item, full }: { item: Item; full: boolean }) {
    if (item.id && state.pool.has(item.id)) {
      state.pool.get(item.id)!.item.set(item, full);
    }
  },
  /**
   * Add either an Item or a Note to the cache kept in the pool.
   * @param state The state of the tree. Should be passed in by Vuex.
   * @param param A parameter representing the item and its parent where item
   *              is an Item or is a Note
   */
  add(state: StateType, item: Item) {
    state.pool.set(item.id, new CacheItem(item, false));
    state.pool.get(item.parent)!.children.push(item.id);
  },
  replace(
    state: StateType,
    { target, replacement }: { target: string; replacement: string }
  ): void {
    if (state.pool.has(target)) {
      let cacheItem: CacheItem = state.pool.get(target)!;
      cacheItem.item.get().id = replacement;
      cacheItem.children.forEach(
        s => (state.pool.get(s)!.item.get().parent = replacement)
      );
      let childList = state.pool.get(cacheItem.item.get().parent)!.children;
      childList[childList.indexOf(target)] = replacement;
      state.pool.set(replacement, cacheItem);
      state.pool.delete(target);
      if (state.openNotes.includes(target)) {
        state.openNotes[state.openNotes.indexOf(target)] = replacement;
      }
      if (state.focused === target) {
        state.focused = replacement;
      }
      if (state.selected === target) {
        state.selected = replacement;
      }
    }
  },
  open(state: StateType, id: string) {
    if (state.pool.has(id) && !state.openNotes.includes(id)) {
      state.openNotes.push(id);
      localStorage.setItem(
        "openTabs",
        JSON.stringify(saveTabs(state, state.openNotes))
      );
    }
  },
  select(state: StateType, id: string): void {
    if (state.pool.has(id)) {
      state.selected = id;
      state.focused = id;
    }
  },
  focus(state: StateType, id: string): void {
    state.focused = id;
  },
  /**
   * Close a currently open Note.
   * @param state The state of the tree. This should be passed by Vuex.
   * @param id The ID of the open note.
   */
  close(state: StateType, id: string) {
    if (state.openNotes.length) {
      if (state.selected === id) {
        let index = state.openNotes.indexOf(id);
        // The item was not found
        if (index === -1) {
          return;
        }
        if (index < state.openNotes.length - 1) {
          if (state.focused === state.selected) {
            state.focused = state.openNotes[index + 1];
          }
          state.selected = state.openNotes[index + 1];
        } else if (state.openNotes.length - 1) {
          if (state.focused === state.selected) {
            state.focused = state.openNotes[index - 1];
          }
          state.selected = state.openNotes[index - 1];
        } else {
          state.selected = "";
        }
      }
      removeFromArray(state.openNotes, id);
      localStorage.setItem(
        "openTabs",
        JSON.stringify(saveTabs(state, state.openNotes))
      );
    }
  },
  deleteParentRefs: function(state: StateType, id: string) {
    removeFromArray(
      state.pool.get(state.pool.get(id)!.item.get().parent)!.children,
      id
    );
  },
  ...clearActions,
  ...saveActions,
  setCurrentHTML(state: StateType, currentHTML: string) {
    state.currentHTML = currentHTML;
  }
};

export default mutations;
