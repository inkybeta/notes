import { StateType } from "../state";

const clearActions = {
  clearFromPool: function(state: StateType, id: string) {
    state.pool.delete(id);
  },
  clearChildReferences: function(state: StateType, id: string) {
    state.pool
      .get(id)!
      .children.forEach(
        (s: string) => (state.pool.get(s)!.item.get().parent = "")
      );
  }
};

export default clearActions;
export { clearActions };
