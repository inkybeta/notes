import { StateType } from "./state";
import { Note, ItemType } from "../../models";

let getters = {
  /**
   * Get the list of currently open tabs in the system.
   */
  tabs: function(state: StateType): Note[] {
    return state.openNotes.map(s => state.pool.get(s)!.item.get()! as Note);
  },

  /**
   * Get the items in the focused directory
   */
  focusedDirectory: function(state: StateType): string {
    return state.focused
      ? state.pool.get(state.focused)!.item.get().type == ItemType.DIRECTORY
        ? state.focused
        : state.pool.get(state.focused)!.item.get().parent
      : "";
  },

  /**
   * Get an item in the cache by its ID.
   */
  item: function(state: StateType) {
    return (id: string) => state.pool.get(id)!.item.get();
  }
};

export default getters;
