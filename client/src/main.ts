// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "./store";
import development from "./development";

Vue.config.productionTip = development;
Vue.config.devtools = development;

var config = {
  apiKey: "AIzaSyCYaZs7_iOGb_ogELbLibP92JcLjWhl0cA",
  authDomain: "notes-a13a2.firebaseapp.com",
  databaseURL: "https://notes-a13a2.firebaseio.com",
  projectId: "notes-a13a2",
  storageBucket: "notes-a13a2.appspot.com",
  messagingSenderId: "286489352385"
};

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>",
  store: store,
  beforeCreate() {
    this.$store.dispatch("services/configureAuthentication", config);
  }
});
