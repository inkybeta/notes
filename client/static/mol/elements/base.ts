function createCircleElement(x: number, y: number, element: string) {
    let svgGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
    let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");;
    circle.setAttribute("cx", x.toString());
    circle.setAttribute("cy", y.toString());
    circle.setAttribute("r", "15px");
    circle.setAttribute("class", "element-circle")
    let text = document.createElementNS("http://www.w3.org/2000/svg", "text");
    text.setAttribute("x", "50%");
    text.setAttribute("y", "50%");
    text.setAttribute("text-anchor", "middle")
    text.setAttribute("stroke", "#f0f0f0")
    text.setAttribute("stroke-width", "2px");
    text.setAttribute("dy", ".3em");
    text.innerHTML = element;
    svgGroup.appendChild(circle);
    svgGroup.appendChild(text);
    return svgGroup;
}

export { createCircleElement }