import interact from "interactjs"
import { createCircleElement } from "./elements";

document.getElementById("add-element").onclick = (event: Event) => {
    addDraggableElement(10, 10);
}

function addDraggableElement(x, y) {
    /**let svgElement = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    svgElement.innerHTML = "C";
    svgElement.setAttribute("cx", x)
    svgElement.setAttribute("cy", y)
    svgElement.setAttribute("r", "10px")
    svgElement.classList.add(".draggable-element");**/
    let svgElement = createCircleElement(10, 10, "C");
    document.getElementById("draggable-pad").appendChild(svgElement);
    interact(svgElement).draggable({
        maxPerElement: Infinity,
        restrict: {
            restriction: "parent",
            endOnly: true,
            elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
        },
        onmove(event) {
            let target = event.target;
            // keep the dragged position in the data-x/data-y attributes
            let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
            let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }
    })
}

export { addDraggableElement }