import torch
import torch.nn as nn

class TextModel(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(TextModel, self)
        self.hidden_size = hidden_size
        self.i2h = nn.Linear(n_categories + input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(n_categories + input_size + hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.LogSoftmax(dim=1)
    
    def forward(self):
        pass

    def loss(self):
        pass
