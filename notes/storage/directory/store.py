from abc import abstractmethod, ABCMeta
from notes.models.errors import StandardError
from notes.models.simple import Item
from notes.models.directory import Directory
from typing import Union
import uuid


class InvalidDirectoryMovementError(StandardError):
    def __init__(self):
        StandardError.__init__(self, 412, "directory/movement/invalid", "Cannot move directory as specified",
                               "You can't move the directory like that")


class DirectoryIntegrityError(StandardError):
    def __init__(self):
        StandardError.__init__(self, 412, "directory/integrity", "The operation cannot be performed as it is invalid",
                               "You can't move the directory like that")


class InvalidDirectory(StandardError):
    def __init__(self, path: Union[str, uuid.UUID], name: str = None):
        if isinstance(path, uuid.UUID):
            StandardError.__init__(self, 412, "directory/invalid",
                                   f"The directory ID {str(path.hex)} is not a valid directory",
                                   "That's not a valid directory.")
        elif name:
            StandardError.__init__(self, 412, "directory/integrity", f"The directory {path + name} is not valid",
                                   "That's not a valid directory.")
        else:
            StandardError.__init__(self, 412, "directory/integrity", f"The directory {path} is not valid",
                                   "That's not a valid directory.")


class DirectoryStore(metaclass=ABCMeta):
    @abstractmethod
    def commit(self) -> None:
        """
        Commit changes to the store.
        """
        pass

    @abstractmethod
    def rollback(self):
        """
        Rollback any updates to the store. Any changes will be lost and a new transaction will be started instead.
        """
        pass

    @abstractmethod
    def get_directory_items(self, user_id: uuid.UUID, directory_id: uuid.UUID) -> [Item]:
        """
        Get all children of the directory as Items.
        :param user_id: the user_id of the client accessing this information
        :param directory_id: the directory_id of the parent_id
        :return: a list of Items representing directory children.
        """
        pass

    @abstractmethod
    def add_directory(self, user_id: uuid.UUID, directory: Directory):
        """
        Add a directory to the store
        :param user_id: the user_id of the client modifying the store
        :param directory: the directory to be added with all fields filled
        :return: a boolean representing whether the upload was successful
        """
        pass

    @abstractmethod
    def add_directories(self, user_id: uuid.UUID, directories: [Directory]):
        """
        Add multiple directories to the store
        :param uuid.UUID user_id:
        :param uuid.UUID directories:
        :return:
        """
        pass

    @abstractmethod
    def remove_directory(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        """

        :param user_id:
        :param directory_id:
        :return:
        """
        pass

    @abstractmethod
    def remove_directories(self, user_id: uuid.UUID, directory_ids: [uuid.UUID]):
        pass

    @abstractmethod
    def move_directory(self, user_id, directory_id, directory):
        """
        Updates the properties of a directory. This feature does not update the name of a directory. Use
        rename_directory for that instead. Instead, this should be used to change the name of the directory. The user
        should never be able to set the path of the directory directly. Query the directory ID of the suggested move
        and change the path accordingly to the format of
        new_path = new_parent_path + new_parent_directory_name
        :param user_id: the user_id of the user performing the action
        :param directory_id: the directory_id of the directory being moved
        :param directory: the properties of the new directory as a Directory object with a new path and new parent_id
            set.
        :return: a boolean indicating the success state of the operation
        """
        pass

    @abstractmethod
    def rename_directory(self, user_id, directory_id, name):
        """
        Renames a directory with a new given name. This sets a new name for the directory and updates all subdirectories
        accordingly.
        :param user_id: the user_id of the user performing the action
        :param directory_id: the id the directory being renamed.
        :param name: the new name of the directory
        :return: a boolean indicating the success state of the rename operation
        :raises: DirectoryIntegrityError
        :raises:
        """
        pass

    @abstractmethod
    def get_directory(self, user_id, directory_id, path=None):
        """

        Args:
            user_id:
            directory_id:
            path:

        Raises:


        Returns:

        """
        pass

    @abstractmethod
    def get_children(self, user_id, directory_id) -> [Directory]:
        """
        Get children as a list of Items
        :param user_id: the id of the user that the directory is associated with
        :param directory_id: the id of the parent directory of all children
        :return: a list of children
        """
        pass

    @abstractmethod
    def get_directory_id(self, user_id, path, name=None):
        """
        Gets a directory id based on its path.
        :param user_id: the id of the user that the directory is associated with
        :param path: the path to the directory (both excluding and including the name of the directory. see below
            for details)
        :param name: the name of the directory. if this is not specified, then the last directory specified in path
            will be used instead. (e.g. if name is None and path is /A/B/C, then this method will look for the directory
            with path /A/B and name C
        :return: a uuid representing the directory ID.
        """
        pass
