"""
This module defines a database store for getting user directories and the like. Due to the number of dependencies on
this module, extra verification is necessary at the store level to ensure the database of directories maintains its
integrity.
The directory table is defined as the following:
user_id: the user id associated with the directory
directory_id: the directory id
parent_id: the id of the parent directory. this is null if there is no parent
path: the path leading to the directory. this must end in a slash and maintain unix path like parameters with spaces
    allowed
name: the name of the directory
"""
import asyncio
import uuid
from typing import List, Optional

import asyncpg

from notes.models.directory import Directory
from notes.models.simple import Item
from notes.storage.directory.store import DirectoryStore, InvalidDirectoryMovementError, DirectoryIntegrityError
from notes.tools.io.directory import extract_path, verify_path, INCORRECT_PATH_STYLE, MISSING_END_SLASH


class PostgresDirectoryStore(DirectoryStore):

    def __init__(self, conn: asyncpg.Connection, timeout: int = 5000):
        self.__conn = conn
        self.__transaction = self.__conn.transaction()
        self.__started = False
        self.__timeout = timeout

    async def __start(self):
        if not self.__started:
            await self.__transaction.start()
            self.__started = True

    async def commit(self):
        await asyncio.shield(self.__transaction.commit())
        self.__transaction = self.__conn.transaction()
        self.__started = False

    async def rollback(self):
        await self.__transaction.rollback()
        self.__transaction = self.__conn.transaction()
        self.__started = False

    async def add_directory(self, user_id: uuid.UUID, directory: Directory):
        await self.__start()
        assert "/" not in directory.name

        if verify_path(directory.path) == INCORRECT_PATH_STYLE:
            raise DirectoryIntegrityError()
        elif verify_path(directory.path) == MISSING_END_SLASH:
            directory.path += "/"

        if directory.parent_id is not None:
            path = await self.__conn.fetchrow(
                "SELECT path || name || '/' FROM directories WHERE user_id=$1 AND directory_id=$2",
                user_id, directory.parent_id, timeout=self.__timeout)
        else:
            path = "/"

        if path[0] != directory.path:
            raise DirectoryIntegrityError()

        await self.__conn.execute("""INSERT INTO directories(user_id, directory_id, parent_id, path, name)
                                     VALUES ($1, $2, $3, $4, $5)""", user_id, directory.directory_id,
                                  directory.parent_id, directory.path, directory.name,
                                  timeout=self.__timeout)
        return True

    async def add_directories(self, user_id: uuid.UUID, directories: List[Directory]):
        await self.__start()
        inserts = [(user_id, directory.directory_id, directory.parent_id, directory.path, directory.name)
                   for directory in directories]
        await self.__conn.executemany("""INSERT INTO directories(user_id, directory_id, parent_id, path, name)
                                         VALUES ($1, $2, $3, $4, $5)""",
                                      inserts,
                                      timeout=self.__timeout)

    async def remove_directory(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()
        await self.__conn.execute("""DELETE FROM directories WHERE user_id=$1 AND directory_id=$2""",
                                  user_id, directory_id, timeout=self.__timeout)
        return True

    async def remove_directories(self, user_id: uuid.UUID, directory_ids: List[uuid.UUID]):
        await self.__start()
        deletes = [(user_id, d) for d in directory_ids]
        await self.__conn.executemany("""DELETE FROM directories WHERE user_id=$1 AND directory_id=$2""", deletes,
                                      timeout=self.__timeout)

    async def move_directory(self, user_id: uuid.UUID, directory_id: uuid.UUID, directory: Directory):
        """
        Updates the properties of a directory. This feature does not update the name of a directory. Use
        rename_directory for that instead. Instead, this should be used to change the name of the directory. The user
        should never be able to set the path of the directory directly. Query the directory ID of the suggested move
        and change the path accordingly to the format of
        new_path = new_parent_path + new_parent_directory_name
        :param user_id: the user_id of the user performing the action
        :param directory_id: the directory_id of the directory being moved
        :param directory: the properties of the new directory as a Directory object with a new path and new parent_id
            set.
        :return: a boolean indicating the success state of the operation
        """
        await self.__start()

        # Ensure basic path integrity
        assert directory.name and "/" not in directory.name

        # Ensure the directory exists and fetch the directory to clean from the name
        old_directory = await self.__conn.fetchrow("""SELECT COUNT(*) AS row_count, path || name AS full_path 
                                                      FROM directories 
                                                      WHERE user_id=$1 AND directory_id=$2""",
                                                   user_id, directory_id)

        if old_directory["row_count"] != 1:
            return False

        old_full_path = old_directory["full_path"]

        # Verify that the directory given is a valid directory and that the new path is a valid path
        new_directory = await self.__conn.fetchrow("""SELECT path || name AS full_path FROM directories
                                                      WHERE user_id=$1 AND directory_id=$2""",
                                                   user_id, directory.parent_id)
        new_full_path = new_directory["full_path"] + "/" + directory.name

        # Set the new directory path
        subdirectories = await self.__conn.fetch("""
            WITH RECURSIVE subdirectories(directory_id, parent_id, path) AS (
                SELECT directory_id, parent_id, path FROM directories WHERE user_id=$1 AND parent_id = $2 OR 
                  ($1 IS NULL AND parent_id IS NULL) 
                UNION 
                SELECT d.directory_id, d.parent_id, d.path FROM subdirectories s, directories d 
                  WHERE d.parent_id = s.directory_id
            )
            SELECT * FROM subdirectories
        """, user_id, directory_id)

        for s in subdirectories:
            stripped_path = s["path"][len(old_full_path):]
            new_path = new_full_path + stripped_path
            if s["directory_id"] == directory.parent_id:
                raise InvalidDirectoryMovementError()
            s["path"] = new_path

        # Update subdirectories
        await self.__conn.executemany("UPDATE directories SET path=$1 WHERE user_id=$2 AND directory_id=$3",
                                      [(s["path"], user_id, s["directory_id"]) for s in subdirectories])

        # Update the actual directory
        await self.__conn.execute("UPDATE directories SET path=$1 WHERE user_id=$2 AND directory_id=$3",
                                  new_directory["full_path"] + "/")

        return True

    async def rename_directory(self, user_id: uuid.UUID, directory_id: uuid.UUID, name: str) -> bool:
        """
        Renames a directory with a new given name. This sets a new name for the directory and updates all subdirectories
        accordingly.
        :param user_id: the id of the associated user
        :param directory_id: the id of the directory being changed
        :param name: the name of the directory
        :return: a boolean representing the success state of the operation
        """
        await self.__start()

        assert "/" not in name

        old_directory = await self.__conn.fetchrow("SELECT path || name || '/' AS full_path, path FROM directories "
                                                   "WHERE user_id=$1 AND directory_id=$1", user_id, directory_id)

        strip = old_directory["full_path"]
        new_path = old_directory["path"] + name + "/"

        subdirectories = await self.__conn.fetch("""
            WITH RECURSIVE subdirectories(directory_id, parent_id, path) AS (
                SELECT directory_id, parent_id, path FROM directories WHERE user_id=$1 AND parent_id = $2 OR 
                  ($1 IS NULL AND parent_id IS NULL) 
                UNION 
                SELECT d.directory_id, d.parent_id, d.path FROM subdirectories s, directories d 
                  WHERE d.parent_id = s.directory_id
            )
            SELECT * FROM subdirectories
        """, user_id, directory_id)

        for subdirectory in subdirectories:
            stripped_path = subdirectory["path"][len(strip):]
            subdirectory["path"] = new_path + stripped_path

        await self.__conn.executemany("UPDATE directories SET path=$1 WHERE user_id=$2 AND directory_id=$3",
                                      [(s["path"], user_id, s["directory_id"]) for s in subdirectories],
                                      timeout=self.__timeout)

        await self.__conn.execute("UPDATE directories SET name=$1 WHERE user_id=$2 AND directory_id=$3",
                                  name, user_id, directory_id)
        return True

    async def get_directory_items(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()

        items = await self.__conn.fetch("SELECT directory_id, name, parent_id FROM directories WHERE user_id=$1 AND "
                                        "(parent_id=$2 OR (parent_id IS NULL AND $2 IS NULL))", user_id, directory_id)
        return [Item(i["directory_id"], i["name"], i["parent_id"]) for i in items]

    async def get_directory(self, user_id: uuid.UUID, directory_id: uuid.UUID = None,
                            path: str = None, name: str = None) -> Optional[Directory]:
        """
        Gets a single directory based on the provided properties. Either
        :param user_id: the id of the associated user
        :param directory_id: the id of the directory [optional]
        :param path: the path of the directory [optional]
        :param name: the name of the directory [optional]
        :return: a directory object if it exists. None if returned otherwise
        """
        await self.__start()

        if directory_id is None:
            return Directory(user_id, None, None, "", "/")

        if directory_id:
            directory = await self.__conn.fetchrow("SELECT * FROM directories WHERE user_id=$1 AND "
                                                   " (directory_id=$2 OR (directory_id IS NULL AND $2 IS NULL))",
                                                   user_id, directory_id,
                                                   timeout=self.__timeout)
            if not directory:
                return None
            return Directory(directory["user_id"], directory["directory_id"], directory["parent_id"], directory["path"],
                             directory["name"])
        elif path:
            if not name:
                separated = extract_path(path)
                path = separated.path
                name = separated.name
            directory = await self.__conn.fetchrow("SELECT * FROM directories WHERE path=$1 AND name=$2",
                                                   path, name,
                                                   timeout=self.__timeout)
            if not directory:
                return None
            return Directory(directory["user_id"], directory["directory_id"], directory["parent_id"],
                             directory["path"],
                             directory["name"])

        raise TypeError

    async def get_children(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID]) -> [Directory]:
        await self.__start()
        assert user_id is not None
        results = await self.__conn.fetch("SELECT * FROM directories "
                                          "WHERE user_id=$1 AND (parent_id=$2 OR ($2 IS NULL AND parent_id IS NULL))",
                                          user_id, directory_id)
        directories = []
        for result in results:
            directories.append(Directory(result["user_id"], result["directory_id"], result["parent_id"], result["path"],
                                         result["name"]))
        return directories

    async def get_directory_id(self, user_id: uuid.UUID, path: str, name: str = None) -> Optional[uuid.UUID]:
        """
        Gets a directory id based on its path.
        :param user_id: the id of the user that the directory is associated with
        :param path: the path to the directory (both excluding and including the name of the directory. see below
            for details)
        :param name: the name of the directory. if this is not specified, then the last directory specified in path
            will be used instead. (e.g. if name is None and path is /A/B/C, then this method will look for the directory
            with path /A/B and name C
        :return: a uuid representing the directory ID.
        """
        await self.__start()
        path = path.rstrip("/ ")
        if path == "":
            return None
        if name is None:
            index = path.rfind('/')
            name = path[index + 1:]
            path = path[:index + 1]
        result = await self.__conn.fetchrow("SELECT directory_id AS d FROM directories "
                                            "WHERE user_id=$1 AND path=$2 AND name=$3",
                                            user_id, path, name)
        if result is None:
            return None
        return result["d"]
