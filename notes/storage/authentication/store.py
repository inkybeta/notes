from abc import ABCMeta, abstractmethod
import uuid


class AuthenticationStorage(metaclass=ABCMeta):
    @abstractmethod
    def commit(self):
        pass

    @abstractmethod
    def rollback(self):
        pass

    @abstractmethod
    async def register(self, user):
        pass

    @abstractmethod
    async def login(self, email, password):
        pass

    @abstractmethod
    async def get_user_by_email(self, email):
        pass

    @abstractmethod
    async def get_user(self, user_id: uuid.UUID):
        pass
