import asyncpg
import asyncio
from notes.models.auth import User
from notes.storage.authentication.store import AuthenticationStorage
import uuid


class PostgresAuthenticationStorage(AuthenticationStorage):
    def __init__(self, connection: asyncpg.Connection, timeout=1000):
        self.__conn = connection
        self.__started = False
        self.__transaction = self.__conn.transaction()
        self.__timeout = timeout

    async def __start(self):
        if not self.__started:
            self.__started = True
            await self.__transaction.start()

    async def commit(self):
        await asyncio.shield(self.__transaction.commit())
        self.__transaction = self.__conn.transaction()
        self.__started = False

    async def rollback(self):
        await asyncio.shield(self.__transaction.rollback())
        self.__transaction = self.__conn.transaction()
        self.__started = False

    async def register(self, user: User):
        await self.__start()
        await self.__conn.execute("INSERT INTO auth.users(user_id, email, first_name, middle_name, last_name, "
                                  "authentication_type) VALUES($1, $2, $3, $4, $5, $6)",
                                  user.user_id, user.email, user.first_name, user.middle_name,
                                  user.last_name,
                                  int(user.authentication_type.value), timeout=self.__timeout)
        return True

    async def login(self, email: str, password: str):
        pass

    async def get_user_by_email(self, email):
        await self.__start()
        row = await self.__conn.fetchrow("SELECT * FROM auth.users WHERE email=$1", email)
        if not row:
            return None
        return User(user_id=row["user_id"], email=row["email"], first_name=row["first_name"],
                    last_name=row["last_name"], middle_name=row["middle_name"],
                    authentication_type=row["authentication_type"])

    async def get_user(self, user_id: uuid.UUID):
        await self.__start()
        row = await self.__conn.fetchrow("SELECT * FROM auth.users WHERE user_id=$1", user_id)
        if not row:
            return None
        return User(user_id=row["user_id"], email=row["email"], first_name=row["first_name"],
                    last_name=row["last_name"], middle_name=row["middle_name"],
                    authentication_type=row["authentication_type"])
