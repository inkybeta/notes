import datetime
import uuid
from typing import List, Tuple, Union

from notes.models import ArticleNote, Note, NoteUpdate, NoteInformation, Item
from abc import ABCMeta
from typing import Optional


class NoteStore(metaclass=ABCMeta):
    async def get_note_information(self, user_id: uuid.UUID, note_id: uuid.UUID, keep_limit: int = None) -> \
            NoteInformation:
        raise NotImplementedError

    async def delete_previous_updates(self, user_id: uuid.UUID, note_id: uuid.UUID,
                                      earliest_date: datetime.datetime = None, keep_limit=None):
        """
        Delete old updates from the database.
        :param user_id: the user id
        :param note_id: the note id
        :param earliest_date: the earliest date that we wish to keep
        :param keep_limit: the limit to the number we should keep.
        :return:
        """
        raise NotImplementedError

    async def add_notes(self, user_id: uuid.UUID, notes: Union[Note, List[Note]]) -> bool:
        """
        Add notes or a note to the database.
        :param user_id: the user id of the associated user
        :param notes: the list of notes to add to the database. they should contain both user_id and the node_id already
            filled in
        :returns: a boolean determining if the transaction failed or not
        """
        raise NotImplementedError

    async def get_path(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def remove_note(self, user_id: uuid.UUID = None, note_id: uuid.UUID = None, note: Note = None) -> bool:
        raise NotImplementedError

    async def remove_notes(self, user_id: uuid.UUID, notes: Union[Note, List[Note], List[uuid.UUID]]):
        raise NotImplementedError

    async def move_note(self, user_id: uuid.UUID, note_id: uuid.UUID, directory_id: uuid.UUID):
        raise NotImplementedError

    async def update_note(self, note: Note, update: NoteUpdate):
        """
        Updates the given note with the update. However, the update is not actually applied (apply_update is not
        called). Rather, the update is merely logged into the database.
        :param note: the updated note
        :param update: the update that was applied
        """
        raise NotImplementedError

    async def update_notes(self, notes: List[Tuple[Note, NoteUpdate]]):
        raise NotImplementedError

    async def get_partial_updates(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_note(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_notes(self, user_id: uuid.UUID, note_ids: Union[uuid.UUID, List[uuid.UUID]]):
        raise NotImplementedError

    async def get_note_item(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_directory_note_items(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        raise NotImplementedError

    async def get_directory_notes(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID]):
        raise NotImplementedError

    async def list_notes(self, user_id: uuid.UUID):
        raise NotImplementedError

    async def commit(self):
        raise NotImplementedError

    async def rollback(self):
        raise NotImplementedError


class ArticleNoteStore(metaclass=ABCMeta):
    async def get_note_information(self, user_id: uuid.UUID, note_id: uuid.UUID, keep_limit: int = None) \
            -> NoteInformation:
        raise NotImplementedError

    async def get_path(self, user_id: uuid.UUID, note_id: uuid.UUID) -> [Item]:
        raise NotImplementedError

    async def delete_previous_updates(self, user_id: uuid.UUID, note_id: uuid.UUID,
                                      earliest_date: datetime.datetime = None, keep_limit=None):
        raise NotImplementedError

    async def add_article_notes(self, notes: Union[ArticleNote, List[ArticleNote]]):
        raise NotImplementedError

    async def remove_article_note(self, user_id: uuid.UUID = None, note_id: uuid.UUID = None, note: ArticleNote = None):
        raise NotImplementedError

    async def remove_article_notes(self, user_id: uuid.UUID,
                                   notes: Union[ArticleNote, List[ArticleNote], List[uuid.UUID]]):
        raise NotImplementedError

    async def move_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, directory_id: uuid.UUID):
        raise NotImplementedError

    async def update_article_note(self, note: ArticleNote, update: NoteUpdate):
        raise NotImplementedError

    async def update_article_notes(self, notes: List[Tuple[ArticleNote, NoteUpdate]]):
        raise NotImplementedError

    async def get_partial_article_updates(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_article_notes(self, user_id: uuid.UUID, note_ids: List[uuid.UUID]):
        raise NotImplementedError

    async def get_article_note_item(self, user_id: uuid.UUID, note_id: uuid.UUID):
        raise NotImplementedError

    async def get_directory_article_note_items(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        raise NotImplementedError

    async def list_article_notes(self, user_id: uuid.UUID, limit: int, skip: int):
        raise NotImplementedError

    async def get_directory_article_notes(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID]):
        raise NotImplementedError

    async def commit(self):
        raise NotImplementedError

    async def rollback(self):
        raise NotImplementedError
