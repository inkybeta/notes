import asyncio
import datetime
import uuid
from typing import List, Tuple, Union, Optional

import asyncpg

from notes.models import Note, NoteUpdate, ArticleNote, Item, NoteInformation
from notes.storage.notes.store import ArticleNoteStore


class PostgresArticleStore(ArticleNoteStore):

    def __init__(self, connection: asyncpg.Connection):
        self.connection = connection
        self.transaction = connection.transaction()
        self.__started = False

    async def __start(self):
        if not self.__started:
            await self.transaction.start()
            self.__started = True

    @staticmethod
    def __map(r: asyncpg.Record) -> ArticleNote:
        return ArticleNote(r["note_id"], r["user_id"], r["title"], r["content"], r["timestamp"], created=r["created"],
                           tags=r["tags"], url=r["url"], published=r["published"], directory_id=r["directory_id"])

    @staticmethod
    def __map_item(r: asyncpg.Record) -> Item:
        return Item(r["note_id"], r["title"], r["timestamp"])

    @staticmethod
    def __map_note_information(r: asyncpg.Record) -> NoteInformation:
        return NoteInformation(r["c"], r["oldest"])

    async def get_note_information(self, user_id: uuid.UUID, note_id: uuid.UUID, keep_limit: int = None) \
            -> NoteInformation:
        if not keep_limit:
            keep_limit = 10
        information = await self.connection.fetchrow("SELECT COUNT(*) as c, min(timestamp) as oldest FROM "
                                                     "(SELECT timestamp FROM partial_updates WHERE user_id=$1 AND "
                                                     "note_id=$2 ORDER BY timestamp ASC LIMIT $3) info;",
                                                     user_id, note_id, keep_limit)
        return PostgresArticleStore.__map_note_information(information)

    async def get_path(self, user_id: uuid.UUID, note_id: uuid.UUID) -> Optional[List[Item]]:
        await self.__start()
        result = await self.connection.fetchrow("SELECT directory_id FROM article_notes WHERE user_id=$1 AND "
                                                "note_id=$2", user_id, note_id)
        if not result:
            return None
        rows = await self.connection.fetch("WITH RECURSIVE path_chain(directory_id, title, parent_id) AS ("
                                           "SELECT directory_id, name, parent_id FROM directories WHERE "
                                           "user_id=$1 AND directory_id=$2 "
                                           "UNION ALL "
                                           "SELECT d.directory_id::uuid, d.name::text, d.parent_id::uuid "
                                           "FROM directories d, path_chain pct "
                                           "WHERE d.directory_id::uuid = pct.parent_id::uuid "
                                           ") SELECT * FROM path_chain;",
                                           user_id, result["directory_id"])
        items = []
        for row in rows:
            items.append(Item(row["directory_id"], row["title"], row["parent_id"]))
        items.reverse()
        return items

    async def delete_previous_updates(self, user_id: uuid.UUID, note_id: uuid.UUID,
                                      earliest_date: datetime.datetime = None, keep_limit=None):
        if not earliest_date:
            note_information = await self.get_note_information(user_id, note_id, keep_limit)
            earliest_date = note_information.earliest_edit_time
        count = await self.connection.execute("DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2 "
                                              "AND timestamp < $3",
                                              user_id, note_id, earliest_date)
        return count

    async def add_article_notes(self, notes: Union[ArticleNote, List[ArticleNote]]):
        await self.__start()
        if type(notes) is ArticleNote:
            notes = [notes]
        insert = [(note.note_id, note.user_id, note.title,
                   note.timestamp, note.created, note.content, note.tags, note.url, note.published, note.authors,
                   note.directory_id) for note in notes]
        try:
            await self.connection.executemany("""
                    INSERT INTO article_notes(note_id, user_id, title, timestamp, created, content, tags, url, 
                    published, authors, directory_id)
                    VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
                """, insert)
            return True
        except ConnectionError:
            return False

    async def remove_article_note(self, user_id: uuid.UUID = None, note_id: uuid.UUID = None, note: ArticleNote = None):
        await self.__start()
        if user_id and note_id:
            await self.connection.execute("""
                    DELETE FROM article_notes WHERE user_id=$1 AND note_id=$2;
                """, user_id, note_id)
            await self.connection.execute("""
                    DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2;
                """, user_id, note_id)

        elif note and note.user_id and note.note_id:
            await self.connection.execute("""
                    DELETE FROM article_notes WHERE user_id=$1 AND note_id=$2;
                """, note.user_id, note.note_id),
            await self.connection.execute("""
                    DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2
                """, user_id, note_id)
        else:
            raise TypeError

    async def remove_article_notes(self, user_id: uuid.UUID,
                                   notes: Union[ArticleNote, List[ArticleNote], List[uuid.UUID]]):
        await self.__start()
        if type(notes) is List[uuid.UUID]:
            await self.connection.execute("""
                DELETE FROM article_notes WHERE user_id=$1 AND note_id=any($2::uuid[]);
            """, user_id, notes)
            await self.connection.execute("""
                DELETE FROM partial_updates WHERE user_id=$1 AND note_id=any($2::uuid[])
            """)
        elif type(notes) is Note:
            notes = [notes]
        if type(notes) is List[Note]:
            pairs = [note.note_id for note in notes]
            await self.connection.execute("""
                DELETE FROM article_notes WHERE user_id=$1 AND note_id=any($2::uuid[]);
            """, user_id, pairs)
            await self.connection.execute("""
                DELETE FROM partial_updates WHERE user_id=$1 AND note_id=any($2::uuid[])
            """, user_id, pairs)

    async def move_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()
        await self.connection.execute("UPDATE article_notes SET directory_id=$1 WHERE user_id=$1 AND note_id=$2",
                                      directory_id, user_id, note_id)
        return True

    async def update_article_note(self, note: ArticleNote, update: NoteUpdate):
        await self.__start()
        assert note.note_id == update.note_id
        assert note.user_id == update.user_id
        update_futures = self.connection.execute("""
            UPDATE article_notes SET title=$1, timestamp=$2, content=$3, tags=$4, url=$5, authors=$6, published=$7 
            WHERE user_id=$7 AND note_id=$8;
        """, note.title, note.timestamp, note.content, note.tags, note.url, note.authors, note.published,
                                                 update.user_id, update.note_id)
        insert_futures = self.connection.execute("""
            INSERT INTO partial_updates(user_id, note_id, edit_id, timestamp, title_update, text_updates, named_updates) 
            VALUES($1, $2, $3, $4, $5, $6::json, $7::json)
        """, update.user_id, update.note_id, update.edit_id, update.timestamp, update.title_update,
                                                 update.get_text_updates(), update.get_named_changes())
        await update_futures
        await insert_futures

    async def update_article_notes(self, notes: List[Tuple[ArticleNote, NoteUpdate]]):
        await self.__start()
        updates = [(note.title, note.timestamp, note.content, note.tags, note.url, note.authors, note.published,
                    update.user_id, update.note_id)
                   for note, update in notes]
        inserts = [(update.user_id, update.note_id, update.edit_id, update.timestamp, update.title_update,
                    update.get_text_updates(), update.get_named_changes()) for _, update in notes]
        update_futures = self.connection.executemany("""
            UPDATE article_notes SET title=$1, timestamp=$2, content=$3, tags=$4, url=$5, authors=$6, published=$7 
            WHERE user_id=$7 AND note_id=$8;
        """, updates)
        insert_futures = self.connection.executemany("""
            INSERT INTO partial_updates(user_id, note_id, edit_id, timestamp, title_update, text_updates, named_updates) 
            VALUES($7, $8, $9, $10, $11, $12::json, $13::json)
        """, inserts)
        await update_futures
        await insert_futures

    async def get_partial_article_updates(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetch("SELECT * FROM partial_updates WHERE user_id=$1 AND note_id=$2", user_id,
                                        note_id, timeout=1000)
        return [
            NoteUpdate(record["user_id"], record["note_id"], record["edit_id"], record["timestamp"],
                       record["title_update"], record["text_updates"], kwargs=record["named_changes"])
            for record in r]

    async def get_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetchrow("""SELECT * FROM article_notes WHERE user_id=$1 AND note_id=$2""", user_id,
                                           note_id)
        if r is None:
            return None
        return self.__map(r)

    async def get_article_note_item(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetchrow("SELECT note_id, title, timestamp FROM article_notes WHERE user_id=$1 AND "
                                           "note_id=$2", user_id, note_id)
        if r is None:
            return r
        return self.__map_item(r)

    async def get_directory_article_note_items(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()
        records = await self.connection.fetch(
            "SELECT note_id, title, timestamp FROM article_notes WHERE user_id=$1 AND "
            "(directory_id=$2 OR ($2 IS NULL AND directory_id IS NULL))", user_id, directory_id)
        return [self.__map_item(r) for r in records]

    async def get_article_notes(self, user_id: uuid.UUID, note_ids: Union[uuid.UUID, List[uuid.UUID]]):
        await self.__start()
        if type(note_ids) is uuid.UUID:
            note_ids = [note_ids]
        records = await self.connection.fetch(
            "SELECT * FROM article_notes WHERE user_id=$1 AND note_id=any($2::uuid[])", user_id, note_ids)
        return [self.__map(r) for r in records]

    async def list_article_notes(self, user_id: uuid.UUID, limit: int, skip: int):
        await self.__start()
        records = await self.connection.fetch("SELECT note_id FROM article_notes WHERE user_id=$1 ORDER BY timestamp "
                                              "LIMIT $2 OFFSET $3", user_id, limit, skip)
        return [r["note_id"] for r in records]

    async def get_directory_article_notes(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID]):
        await self.__start()
        records = await self.connection.fetch("SELECT * FROM article_notes WHERE user_id=$1 AND (directory_id=$2 OR "
                                              "(directory_id IS NULL AND $2 IS NULL))",
                                              user_id, directory_id)
        return [r["note_id"] for r in records]

    async def commit(self):
        await asyncio.shield(self.transaction.commit())
        self.transaction = self.connection.transaction()
        self.__started = False

    async def rollback(self):
        await self.transaction.rollback()
        self.transaction = self.connection.transaction()
