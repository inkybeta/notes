import asyncio
import datetime
import uuid
from typing import List, Tuple, Union, Optional

import asyncpg

from notes.models import Note, NoteUpdate, Item
from notes.models.note import NoteInformation
from notes.storage.notes.store import NoteStore


class PostgresNoteStore(NoteStore):

    def __init__(self, connection: asyncpg.Connection):
        self.connection = connection
        self.transaction = connection.transaction()
        self.__started = False

    async def __start(self):
        if not self.__started:
            await self.transaction.start()
            self.__started = True

    @staticmethod
    def __map(r: asyncpg.Record):
        return Note(note_id=r["note_id"],
                    user_id=r["user_id"],
                    title=r["title"],
                    content=r["content"],
                    timestamp=r["timestamp"],
                    created=r["created"],
                    tags=r["tags"],
                    directory_id=r["directory_id"])

    @staticmethod
    def __map_item(r: asyncpg.Record):
        return Item(r["note_id"], r["title"], r["timestamp"])

    @staticmethod
    def __map_note_information(r: asyncpg.Record) -> NoteInformation:
        return NoteInformation(r["c"], r["oldest"])

    async def get_note_information(self, user_id: uuid.UUID, note_id: uuid.UUID, keep_limit: int = None) \
            -> NoteInformation:
        if not keep_limit:
            keep_limit = 10
        information = await self.connection.fetchrow("SELECT COUNT(*) as c, min(timestamp) as oldest FROM "
                                                     "(SELECT timestamp FROM partial_updates WHERE user_id=$1 AND "
                                                     "note_id=$2 ORDER BY timestamp ASC LIMIT $3) info;",
                                                     user_id, note_id, keep_limit)
        return PostgresNoteStore.__map_note_information(information)

    async def delete_previous_updates(self, user_id: uuid.UUID, note_id: uuid.UUID,
                                      earliest_date: datetime.datetime = None, keep_limit=None):
        if not earliest_date:
            note_information = await self.get_note_information(user_id, note_id, keep_limit)
            earliest_date = note_information.earliest_edit_time
        count = await self.connection.execute("DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2 "
                                              "AND timestamp < $3",
                                              user_id, note_id, earliest_date)
        return count

    async def add_notes(self, user_id: uuid.UUID, notes: Union[Note, List[Note]]):
        await self.__start()
        if type(notes) is Note:
            notes = [notes]
        insert = [(note.note_id, user_id, note.title,
                   note.timestamp, note.created, note.content, note.tags, note.directory_id) for note in notes]
        await self.connection.executemany("""
                    INSERT INTO notes(note_id, user_id, title, timestamp, created, content, tags, directory_id)
                    VALUES($1, $2, $3, $4, $5, $6, $7, $8)
                """, insert)
        return True

    async def get_path(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        result = await self.connection.fetchrow("SELECT directory_id FROM notes WHERE user_id=$1 AND note_id=$2",
                                                user_id, note_id)
        if not result:
            return None
        rows = await self.connection.fetch("WITH RECURSIVE path_chain(directory_id, title, parent_id) AS ("
                                           "SELECT directory_id, name, parent_id FROM directories WHERE "
                                           "user_id=$1 AND directory_id=$2 "
                                           "UNION ALL "
                                           "SELECT d.directory_id::uuid, d.name::text, d.parent_id::uuid "
                                           "FROM directories d, path_chain pct "
                                           "WHERE d.directory_id::uuid = pct.parent_id::uuid "
                                           ") SELECT * FROM path_chain;",
                                           user_id, result["directory_id"])
        items = []
        for row in rows:
            items.append(Item(row["directory_id"], row["title"], row["parent_id"]))
        items.reverse()
        return items

    async def move_note(self, user_id: uuid.UUID, note_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()
        await self.connection.execute("UPDATE notes SET directory_id=$1 WHERE user_id=$1 AND note_id=$2", directory_id,
                                      user_id, note_id)
        return True

    async def remove_note(self, user_id: uuid.UUID = None, note_id: uuid.UUID = None, note: Note = None):
        await self.__start()
        if user_id and note_id:
            await self.connection.execute("""
                        DELETE FROM notes WHERE user_id=$1 AND note_id=$2;
                    """, user_id, note_id),
            await self.connection.execute("""
                        DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2
                    """, user_id, note_id)

        elif note and note.user_id and note.note_id:
            await self.connection.execute("""
                        DELETE FROM notes WHERE user_id=$1 AND note_id=$2;
                """, note.user_id, note.note_id),
            await self.connection.execute("""
                        DELETE FROM partial_updates WHERE user_id=$1 AND note_id=$2
                """, note.user_id, note.note_id)
        else:
            raise TypeError

    async def remove_notes(self, user_id: uuid.UUID, notes: Union[Note, List[Note], List[uuid.UUID]]):
        await self.__start()
        if type(notes) is List[uuid.UUID]:
            await self.connection.execute("""
                    DELETE FROM notes WHERE user_id=$1 AND note_id=any($2::uuid[]);
                """, user_id, notes),
            await self.connection.execute("""
                    DELETE FROM partial_updates WHERE user_id=$1 AND note_id=any($2::uuid[])
                """, user_id, notes)
        elif type(notes) is Note:
            notes = [notes]
        if type(notes) is List[Note]:
            pairs = [note.note_id for note in notes]
            await self.connection.execute("""
                    DELETE FROM notes WHERE user_id=$1 AND note_id=any($2::uuid[]);
                """, user_id, pairs),
            await self.connection.execute("""
                    DELETE FROM partial_updates WHERE user_id=$1 AND note_id=any($2::uuid[]);
                """, user_id, pairs)

    async def update_note(self, note: Note, update: NoteUpdate):
        await self.__start()
        if update:
            assert note.note_id == update.note_id
            assert note.user_id == update.user_id
        update_future = self.connection.execute("""
                UPDATE notes SET title=$1, timestamp=$2, content=$3, tags=$4 WHERE user_id=$5 AND note_id=$6;
            """, note.title, note.timestamp, note.content, note.tags, update.user_id, update.note_id)
        insert_future = self.connection.execute(
            "INSERT INTO partial_updates(user_id, note_id, edit_id, timestamp, title_update,"
            "text_updates, named_updates)"
            "VALUES($1, $2, $3, $4, $5, $6::json, $7::json)", update.user_id, update.note_id, update.edit_id,
            update.timestamp, update.title_update,
            update.get_text_updates(), update.get_named_changes())
        await insert_future
        await update_future

    async def update_notes(self, notes: List[Tuple[Note, NoteUpdate]]):
        await self.__start()

        updates = [(note.title, note.timestamp, note.content, note.tags,
                    update.user_id, update.note_id) for note, update in notes]
        inserts = [(update.user_id, update.note_id, update.edit_id, update.timestamp, update.title_update,
                    update.get_text_updates(), update.get_named_changes()) for note, update in notes]

        update_futures = self.connection.executemany("""
                    UPDATE notes SET title=$1, timestamp=$2, content=$3, tags=$4 WHERE user_id=$5 AND note_id=$6;
                """, updates)
        insert_futures = self.connection.executemany("""
                    INSERT INTO partial_updates(user_id, note_id, edit_id, timestamp, 
                      title_update, text_updates, named_updates) 
                    VALUES($1, $2, $3, $4, $5, $6::json, $7::json)
            """, inserts)

        await update_futures
        await insert_futures

    async def get_partial_updates(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetch("SELECT * FROM partial_updates WHERE user_id=$1 AND note_id=$2", user_id,
                                        note_id,
                                        timeout=1000)
        return [self.__map(record) for record in r]

    async def get_note(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetchrow("""SELECT * FROM notes WHERE user_id=$1 AND note_id=$2""", user_id, note_id)
        if r is None:
            return None
        return self.__map(r)

    async def get_notes(self, user_id: uuid.UUID, note_ids: Union[uuid.UUID, List[uuid.UUID]]):
        await self.__start()
        if type(note_ids) is uuid.UUID:
            note_ids = [note_ids]
        records = await self.connection.fetch("SELECT * FROM notes WHERE user_id=$1 AND note_id=any($2::uuid[])",
                                              (user_id, note_ids))
        return [self.__map(r) for r in records]

    async def get_note_item(self, user_id: uuid.UUID, note_id: uuid.UUID):
        await self.__start()
        r = await self.connection.fetchrow("SELECT note_id, title, timestamp FROM notes WHERE user_id=$1 AND "
                                           "note_id=$2", user_id, note_id)
        if r is None:
            return r
        return self.__map_item(r)

    async def get_directory_note_items(self, user_id: uuid.UUID, directory_id: uuid.UUID):
        await self.__start()
        records = await self.connection.fetch(
            "SELECT note_id, title, timestamp, directory_id FROM notes WHERE user_id=$1 "
            "AND (directory_id=$2 OR ($2 IS NULL AND directory_id IS NULL))", user_id,
            directory_id)
        return [self.__map_item(r) for r in records]

    async def list_notes(self, user_id: uuid.UUID):
        await self.__start()
        records = await self.connection.fetch("SELECT note_id FROM notes WHERE user_id=$1", user_id)
        return [r["note_id"] for r in records]

    async def commit(self):
        await asyncio.shield(self.transaction.commit())
        self.transaction = self.connection.transaction()
        self.__started = False

    async def rollback(self):
        await self.transaction.rollback()
        self.transaction = self.connection.transaction()
        self.__started = False

    async def get_directory_notes(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID]):
        await self.__start()
        records = await self.connection.fetch(
            "SELECT note_id FROM notes WHERE user_id=$1 AND (directory_id=$2 OR (directory_id IS NULL AND $2 IS NULL))",
            user_id, directory_id)
        return [r["note_id"] for r in records]
