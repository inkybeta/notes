import uuid
import datetime
from typing import List
import abc

from notes.models import Note, ArticleNote


class NoteSearch(metaclass=abc.ABCMeta):
    async def find_notes(self, user_id: uuid.UUID, title: str = None, text: str = None, tags: List[str] = None,
                         path: str = None, created_start: datetime.datetime = None,
                         created_end: datetime.datetime = None,
                         start_date: datetime.datetime = None, end_date: datetime.datetime = None,
                         start: int = 0, count: int = 30):
        raise NotImplementedError

    async def add_note(self, user_id: uuid.UUID, note_id: uuid.UUID, note: Note):
        raise NotImplementedError

    async def update_note(self, user_id: uuid.UUID, note_id: uuid.UUID, **kwargs) -> bool:
        raise NotImplementedError

    async def delete_note(self, user_id: uuid.UUID, note_id: uuid.UUID) -> bool:
        raise NotImplementedError


class ArticleNoteSearch(metaclass=abc.ABCMeta):
    async def find_article_notes(self, user_id: uuid.UUID, title: str = None, text=None, tags=None, authors=None,
                                 url: str = None, path: str = None,
                                 start_date: datetime.datetime = None,
                                 end_date: datetime.datetime = None,
                                 start: int = 0, count: int = 30):
        raise NotImplementedError

    async def add_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, note: ArticleNote) -> bool:
        raise NotImplementedError

    async def update_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, **kwargs) -> bool:
        raise NotImplementedError

    async def delete_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID) -> bool:
        raise NotImplementedError
