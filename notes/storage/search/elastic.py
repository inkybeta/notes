import datetime
import uuid
from typing import List

import elasticsearch as es

from notes.models import Item, Note, ArticleNote, SearchItem
from notes.storage.search.elastic_builder import ElasticSearchQueryBuilder
from notes.tools import parse_datetime, parse_uuid
from .search import ArticleNoteSearch, NoteSearch


class DirectorySearch:
    def __init__(self, connection: es.Elasticsearch):
        self.connection = connection
        self.query_builder = ElasticSearchQueryBuilder(connection, "notes,directories")

    @staticmethod
    def __map(hits):
        notes = []
        articles = []
        directories = []
        for i in hits:
            if "type" in i["_source"]:
                if i["_source"]["type"] == 1:
                    notes.append(SearchItem(parse_uuid(i["_id"]), i["_source"]["title"],
                                            parse_uuid(i["_source"]["directory_id"]),
                                            i["highlight"]["content.ngram"],
                                            parse_datetime(i["_source"]["timestamp"])))
                elif i["_source"]["type"] == 2:
                    articles.append(SearchItem(uuid.UUID(i["_id"]), i["_source"]["title"],
                                               parse_uuid(i["_source"]["directory_id"]),
                                               i["highlight"]["content.ngram"],
                                               parse_datetime(i["_source"]["timestamp"])))
            else:
                directories.append(Item(uuid.UUID(i["_id"]), i["_source"]["title"],
                                        parse_uuid(i["_source"]["parent_id"])))
        return {
            "notes": notes,
            "articles": articles,
            "directories": directories
        }

    async def find(self, user_id: uuid.UUID, title: str = None, text: str = None, tags: [str] = None,
                   path: str = None, created_start: datetime.datetime = None,
                   created_end: datetime.datetime = None,
                   url: str = None,
                   authors: [str] = None,
                   start_date: datetime.datetime = None, end_date: datetime.datetime = None,
                   start: int = 0, count: int = 30):
        if not count:
            count = 30
        elif count > 50:
            count = 50
        elif count <= 0:
            count = 1

        builder = self.query_builder
        if count:
            builder.limit(count)
        if start:
            builder.start(start)
        must_match = builder.bool().must()

        if text:
            if text and len(text) < 30:
                must_match.multi_match(text, ["content^2", "content.ngram", "title"])
            else:
                must_match.multi_match(text, ["content^2", "content.ngram"])

        if title:
            must_match.multi_match(title, ["title"])

        if tags:
            must_match.match_terms("tags", tags)

        if path:
            must_match.match_terms("path", path)

        if url:
            must_match.match("url", url)

        if authors:
            must_match.match_terms("authors", authors)

        filter_match = must_match.filter()

        filter_match.match_term("user_id", str(user_id.hex))
        filter_match.highlights().fields("content", number_of_fragments=3) \
            .fields("content.ngram", number_of_fragments=3) \
            .fields("title", number_of_fragments=3)

        result = filter_match.retrieve("_id").retrieve("title").retrieve("timestamp") \
            .retrieve("type").retrieve("directory_id").retrieve("parent_id").execute()
        obj = self.__map(result["hits"]["hits"])
        obj["total"] = result["hits"]["total"]
        return obj


class ElasticNoteSearch(NoteSearch):
    def __init__(self, connection: es.Elasticsearch):
        NoteSearch.__init__(self)
        self.connection = connection
        self.__index = "notes"
        self.query_builder = ElasticSearchQueryBuilder(connection, self.__index)

    async def find_notes(self, user_id: uuid.UUID, title: str = None, text: str = None, tags: List[str] = None,
                         path: str = None, created_start: datetime.datetime = None,
                         created_end: datetime.datetime = None,
                         start_date: datetime.datetime = None, end_date: datetime.datetime = None,
                         start: int = 0, count: int = 30):
        if not title and not text and not tags and not start_date and not end_date:
            raise TypeError
        if not title and text and len(text) < 50:
            title = text
        if not count:
            count = 30
        elif count > 50:
            count = 50
        elif count <= 0:
            count = 1

        builder = self.query_builder
        if count:
            builder.limit(count)
        if start:
            builder.start(start)
        must_match = builder.bool().must()

        if text:
            must_match.multi_match(text, ["content^2", "content.ngram"])

        if title:
            must_match.multi_match(title, ["title"])

        if tags:
            must_match.match_terms("tags", tags)

        if path:
            must_match.match_terms("path", path)

        filter_match = must_match.filter()

        filter_match.match("type", 1)
        filter_match.match_terms("user_id", str(user_id))

        result = filter_match.retrieve("note_id").retrieve("title").retrieve("timestamp").execute()
        results = []
        for hit in result["hits"]["hits"]:
            results.append(Item(uuid.UUID(hit["_id"]), hit["title"], parse_uuid(hit["directory_id"]),
                                parse_datetime(hit["timestamp"])))
        return results

    async def add_note(self, user_id: uuid.UUID, note_id: uuid.UUID, note: Note):
        obj = dict()
        obj.update(vars(note))
        obj["type"] = 1
        obj["user_id"] = user_id.hex
        obj["note_id"] = note_id.hex
        return self.connection.index(self.__index, "doc", obj, id=note_id)["result"] == "created"

    async def update_note(self, user_id: uuid.UUID, note_id: uuid.UUID, **kwargs) -> bool:
        if kwargs["note"]:
            obj = dict()
            obj.update(vars(kwargs["note"]))
            obj["type"] = 1
            obj["user_id"] = user_id.hex
            obj["note_id"] = note_id.hex
            return self.connection.index(self.__index, "doc", obj, id=note_id)["result"] == "updated"
        obj = kwargs
        return self.connection.update(self.__index, "doc", obj, id=note_id)["result"] == "updated"

    async def delete_note(self, user_id: uuid.UUID, note_id: uuid.UUID) -> bool:
        return self.connection.delete(self.__index, "doc", note_id)["result"] == "deleted"


class ElasticArticleNoteSearch(ArticleNoteSearch):

    def __init__(self, connection: es.Elasticsearch):
        self.connection = connection
        self.__index = "notes"
        self.query_builder = ElasticSearchQueryBuilder(connection, "notes")

    async def find_article_notes(self, user_id: uuid.UUID, title: str = None, text=None, tags=None, authors=None,
                                 url: str = None, path: str = None,
                                 start_date: datetime.datetime = None,
                                 end_date: datetime.datetime = None,
                                 start: int = 0, count: int = 30):
        if not title and not text and not tags and not authors and not start_date and not end_date:
            raise TypeError
        if not title and text and len(text) < 50:
            title = text

        if not title and text and len(text) < 50:
            title = text
        if not count:
            count = 30
        elif count > 50:
            count = 50
        elif count <= 0:
            count = 1

        builder = self.query_builder
        if count:
            builder.limit(count)
        if start:
            builder.start(start)

        must_match = builder.bool().must()

        if text:
            must_match.multi_match(text, ["content^2", "content.ngram"])

        if title:
            must_match.multi_match(title, ["title"])

        if tags:
            must_match.match_terms("tags", tags)

        if authors:
            must_match.match_terms("authors", authors)

        if url:
            must_match.match("url", url)

        if path:
            must_match.match_terms("path", path)

        filter_match = must_match.filter()

        filter_match.match("type", 2)
        filter_match.match_terms("user_id", str(user_id))

        result = filter_match.retrieve("note_id").retrieve("title").retrieve("timestamp").execute()
        results = []
        for hit in result["hits"]["hits"]:
            results.append(Item(uuid.UUID(hit["_id"]), hit["title"], parse_uuid(hit["directory_id"]),
                                parse_datetime(hit["timestamp"])))

        return results

    async def add_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, note: ArticleNote) -> bool:
        obj = dict()
        obj.update(vars(note))
        obj["type"] = 2
        obj["user_id"] = user_id.hex
        obj["note_id"] = note_id.hex
        return self.connection.index(self.__index, "doc", obj, id=note_id.hex)["result"] == "created"

    async def update_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID, **kwargs) -> bool:
        if kwargs["note"]:
            obj = dict()
            obj.update(vars(kwargs["note"]))
            obj["type"] = 2
            obj["user_id"] = user_id.hex
            obj["note_id"] = note_id.hex
            return self.connection.index(self.__index, "doc", obj, id=note_id.hex)["result"] == "updated"
        obj = kwargs
        return self.connection.update(self.__index, "doc", obj, id=note_id.hex)["result"] == "updated"

    async def delete_article_note(self, user_id: uuid.UUID, note_id: uuid.UUID) -> bool:
        return self.connection.delete(self.__index, "doc", id=note_id.hex)["result"] == "deleted"
