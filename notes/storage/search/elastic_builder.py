import elasticsearch as es


class ElasticSearchQueryBuilder:
    def __init__(self, connection: es.Elasticsearch, index: str):
        self.connection = connection
        self.__index = index
        self.__body = {}
        self.__property = "query"
        self.__query_mode = ""
        self.__query_clause = ""

    def __get_query_leaf(self):
        if self.__query_clause == "":
            raise NotImplementedError
        if "query" not in self.__body:
            self.__body["query"] = {}

        if self.__query_mode != "":
            if self.__query_mode not in self.__body["query"]:
                self.__body["query"][self.__query_mode] = {}
            mode = self.__body["query"][self.__query_mode]
        else:
            mode = self.__body["query"]

        if self.__query_clause not in mode:
            mode[self.__query_clause] = []

        return mode[self.__query_clause]

    def highlights(self) -> "ElasticSearchQueryBuilder":
        self.__property = "highlight"
        return self

    def fields(self, field: str, **parameters) -> "ElasticSearchQueryBuilder":
        if self.__property not in self.__body:
            self.__body[self.__property] = {}
        if "fields" not in self.__body[self.__property]:
            self.__body[self.__property]["fields"] = {}
        self.__body[self.__property]["fields"][field] = parameters
        return self

    def retrieve(self, field):
        if "_source" not in self.__body:
            self.__body["_source"] = []
        self.__body["_source"].append(field)
        return self

    def limit(self, count: int):
        self.__body["size"] = count
        return self

    def start(self, start: int):
        self.__body["from"] = start
        return self

    def bool(self):
        self.__query_mode = "bool"
        return self

    def must(self):
        self.__query_clause = "must"
        return self

    def filter(self):
        self.__query_clause = "filter"
        return self

    def multi_match(self, query, fields):
        self.__get_query_leaf().append({
            "multi_match": {
                "query": query,
                "fields": fields
            }
        })
        return self

    def match_terms(self, fields, terms):
        if type(fields) is list:
            query = {field: term for field, term in zip(fields, terms)}
        else:
            query = {fields: terms}
        self.__get_query_leaf().append({
            "terms": query
        })
        return self

    def match_term(self, field, term):
        self.__get_query_leaf().append({
            "term": {field: term}
        })
        return self

    def match(self, fields, queries):
        if type(fields) is list:
            query = {field: query for field, query in zip(fields, queries)}
        else:
            query = {fields: queries}
        self.__get_query_leaf().append([{
            "match": query
        }])
        return self

    def execute(self):
        return self.connection.search(self.__index, body=self.__body)
