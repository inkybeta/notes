import elasticsearch as es
from .query import QueryBuilder
from .highlight import HighlightBuilder


class ElasticBuilder:
    def __init__(self, connection: es.Elasticsearch, index: str or [str]):
        if type(index) is list:
            index = ",".join(index)
        self.__index = index
        self.__body = {}
        self.__connection = connection

    def limit(self, count: int):
        self.__body["size"] = count
        return self

    def start(self, start: int):
        self.__body["from"] = start
        return self

    def query(self) -> QueryBuilder:
        pass

    def execute(self):
        return self.__connection.search(self.__index, body=self.__body)
