from typing import Union, Any


class QueryBuilder:
    def __init__(self, query: {str: Any}):
        self.query = query

    def must(self, field: str,
             message: Union[str, Any]):
        if not self.query["must"]:
            self.query["must"] = {}
        self.query["must"][field] = message

    def filter(self, field: str,
               message: Union[str, Any]):
        self.query[field] = message

    def range(self, field: str,
              gte: float = None,
              lte: float = None,
              boost: float = None,
              gt: float = None,
              lt: float = None):
        query = {}
        if gte:
            query["gte"] = gte
        if lte:
            query["lte"] = lte
        if boost:
            query["boost"] = boost
        if gt:
            query["gt"] = gt
        if lt:
            query["lt"] = lt
        self.query["range"][field] = query

    def root(self):
        pass
