from typing import Callable, Any
from aiohttp.web import Request, RouteTableDef


class ConditionalRouteTable(RouteTableDef):
    def __init__(self):
        RouteTableDef.__init__(self)

    def get(self, path, require=None, **kwargs):
        return RouteTableDef.get(path, **kwargs)


async def conditional_route(f: Callable[[Request], Any]) -> Callable[..., Any]:
    async def wrapper(request: Request):
        return await f(request)

    return wrapper
