from typing import Type
from notes.models.errors import ModelTypeError, MissingParameterError, UUIDError, EmailParseError
import uuid
import re


def extract(dictionary, argument: str, t: Type, type_name: str = None, default=None,
            required=True):
    """
    Extract the value of an item in a dictionary an ensure its type
    :param required: whether the argument is required. If it is, throw a MissingParameterError
        if it is not found.
    :param dictionary: the dictionary of values from the model
    :param argument: the argument of the directory
    :param t: the type of the argument
    :param type_name: the name of the type as a string
    :param default: the default value if the argument does not exist
    :return: the value of dictionary[argument] with a guarantee on its type
    """
    f = dictionary[argument] if argument in dictionary else default
    if f and not isinstance(f, t):
        if type_name:
            raise ModelTypeError(argument, type_name)
        else:
            raise ModelTypeError(argument, t.__name__)
    if not f and required and f != "":
        raise MissingParameterError(argument)
    return f


def extract_list(dictionary, argument: str, t: Type, type_name: str = None, default=None,
                 required=True):
    f = dictionary[argument] if argument in dictionary else default
    if f and not isinstance(f, list):
        if type_name:
            raise ModelTypeError(argument, type_name)
        else:
            raise ModelTypeError(argument, f"[{t.__name__}]")
    if not f and required and f != "":
        raise MissingParameterError(argument)
    return f


def parse_uuid(uid: str) -> uuid.UUID:
    """
    Parses a UUID and raises a StandardError if the parsing fails
    :param uid: the uuid as a string
    :return: the parsed UUID
    """
    if uid is None:
        return uid
    try:
        return uuid.UUID(uid)
    except ValueError:
        raise UUIDError()


email_regex = re.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")


def parse_email(email: str):
    if email_regex.match(email):
        return email
    else:
        raise EmailParseError()
