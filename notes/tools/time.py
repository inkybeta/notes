import datetime


def parse_datetime(d: str) -> datetime.datetime:
    try:
        return datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%S.%fZ")
    except ValueError:
        return datetime.datetime.strptime(d, "%Y-%m-%dT%H:%M:%S.%f")
