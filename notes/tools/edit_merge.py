from notes.models import Edit
from typing import List, Deque
from collections import deque


class EditMinimizer:
    def __init__(self):
        self.insert_char_buffer: List[Edit] = []
        self.delete_char_buffer: List[Edit] = []

    def concat_edits(self, edits: List[Edit]) -> None:
        insert_edit_chars: List[Edit] = []
        for i in range(len(edits)):
            if (edits[i].is_delete()):
                break
            else:
                for j in range(len(edits[i].text)):
                    insert_edit_chars.append(
                        Edit(edits[i].action, edits[i].index + j, edits[i].text[j]))
                del j
        insert_delete_chars: List[Edit] = []
        for d in reversed(range(i, len(edits))):
            for j in range(len(edits[d].text)):
                insert_delete_chars.append(
                    Edit(edits[d].action, edits[d].index + j, edits[d].text[j]))
            del j
        del d
        del i
        for edit in insert_edit_chars:
            EditMinimizer.resolve_insert_edit(
                edit, self.insert_char_buffer, self.delete_char_buffer)
        del edit
        for delete in insert_delete_chars:
            EditMinimizer.resolve_delete_edit(
                delete, self.insert_char_buffer, self.delete_char_buffer)
        del delete

    def minimize(self, lossy: bool) -> List[Edit]:
        if lossy:
            return self._minimize_lossy()
        else:
            return self._minimize_non_lossy()

    def _minimize_non_lossy(self) -> List[Edit]:
        return EditMinimizer.collapse_to_words(self.insert_char_buffer, self.delete_char_buffer)

    def _minimize_lossy(self) -> List[Edit]:
        insert_copy: Deque[Edit] = deque(self.insert_char_buffer)
        delete_copy: Deque[Edit] = deque(self.delete_char_buffer)
        preserved_inserts: Deque[Edit] = deque()
        preserved_deletes: Deque[Edit] = deque()
        # These nested loops, while apparently scary, only ever pass over at
        # most twice the number of elements of the smaller deque, worst case
        while insert_copy and delete_copy:
            delete: Edit = delete_copy.pop()
            insert: Edit = insert_copy.pop()
            while insert and insert.index > delete.index:
                preserved_inserts.appendleft(insert)
                insert = insert_copy.pop()
            if insert:
                if insert.index == delete.index:
                    continue
                else:
                    insert_copy.append(insert)
            preserved_deletes.appendleft(delete)
        insert_copy.extend(preserved_inserts)
        delete_copy.extend(preserved_deletes)
        return EditMinimizer.collapse_to_words(insert_copy, delete_copy)

    @staticmethod
    def collapse_to_words(insert_char_buffer: List[Edit], delete_char_buffer: List[Edit]) -> List[Edit]:
        return EditMinimizer.collapse(insert_char_buffer).extend(reversed(EditMinimizer.collapse(delete_char_buffer)))

    @staticmethod
    def collapse(char_buffer: List[Edit]) -> List[Edit]:
        if char_buffer:
            collapsed: Deque[Edit] = deque()
            collapsing: Edit = char_buffer.pop()
            while (char_buffer):
                test: Edit = char_buffer.pop()
                if collapsing.index == char_buffer.index + 1:
                    collapsing = Edit(test.action, test.index,
                                      test.text + collapsing.text)
                else:
                    collapsed.appendleft(collapsing)
                    collapsing = test
            collapsed.appendleft(collapsing)
            return list(collapsed)
        else:
            return char_buffer

    @staticmethod
    def resolve_insert_edit(edit: Edit, insert_buffer: List[Edit], delete_buffer: List[Edit]):
        for delete in delete_buffer:
            if delete.index < edit.index + 1:
                edit.index += 1
            else:
                delete.index += 1
        index: int = -1
        for i in range(len(insert_buffer)):
            if (insert_buffer[i].index + 1 > edit.index):
                index = i
                break
        if index < 0:
            insert_buffer.append(edit)
        else:
            insert_buffer.insert(index, edit)
            for i in range(index + 1, len(insert_buffer)):
                insert_buffer[i].index += 1

    @staticmethod
    def resolve_delete_edit(edit: Edit, insert_buffer: List[Edit], delete_buffer: List[Edit]):
        count: int = 0
        for u in delete_buffer:
            if u.index < edit.index + 1:
                count += 1
                edit.index += 1
        if count < len(delete_buffer):
            delete_buffer.append(edit)
        else:
            delete_buffer.insert(count, edit)
