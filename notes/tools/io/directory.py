import re
from typing import NamedTuple

path_pattern = re.compile("^(?:\\/[^\\/]+)*(\\/)?$")

INCORRECT_PATH_STYLE = 0
MISSING_END_SLASH = 1
CORRECT_PATH = 2


class SeparatedDirectory(NamedTuple):
    path: str
    name: str


def extract_path(path: str) -> SeparatedDirectory:
    path = path.rstrip("/ ")
    if path == "":
        return SeparatedDirectory(path="/", name="")
    index = path.rfind('/')
    name = path[index + 1:]
    path = path[:index + 1]
    return SeparatedDirectory(path, name)


def verify_path(path: str) -> int:
    p = path_pattern.search(path)
    if not p:
        return INCORRECT_PATH_STYLE
    if len(p.groups()) >= 1:
        return CORRECT_PATH
    elif len(p.groups()) == 0:
            return MISSING_END_SLASH
    else:
        return INCORRECT_PATH_STYLE
