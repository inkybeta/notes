from aiohttp.web import middleware, Request
import uuid


def get_request_id(request: Request) -> uuid.UUID:
    return request["request_id"]


@middleware
async def add_request_tagging(request: Request, handler):
    request["request_id"] = uuid.uuid4()
    return await handler(request)
