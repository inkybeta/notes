from aiohttp.web import middleware, Response, Request


def generate_headers(origin: str, max_age: int = 86400) -> dict:
    headers = {
        "Access-Control-Max-Age": str(max_age), "Access-Control-Allow-Origin": origin,
        "Access-Control-Allow-Headers": "Content-Type, Origin, Content-Length, X-Requested-With, Accept, "
                                        "Authorization",
        "Access-Control-Allow-Methods": "DELETE, POST, GET, PUT, PATCH, REQUEST, HEAD, OPTIONS"
    }
    return headers


def enable_cross_origin(origin):
    @middleware
    async def cors_headers(request: Request, handler):
        if request.method == "OPTIONS":
            response = Response(headers=generate_headers(origin))
            return response
        try:
            response: Response = await handler(request)
            response.headers["Access-Control-Allow-Origin"] = origin
        except BaseException as err:
            if "headers" not in dir(err):
                err.headers = generate_headers(origin)
                raise err
            else:
                transient = generate_headers(origin)
                for key, value in transient.items():
                    err.headers[key] = value
                raise err

        return response

    return cors_headers
