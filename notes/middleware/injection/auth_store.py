from aiohttp.web import Request

from notes.storage.authentication.store import AuthenticationStorage


def get_authentication_storage(request: Request) -> AuthenticationStorage:
    return request["authentication_storage"]
