import elasticsearch as es
from aiohttp.web import middleware, Request

from notes.storage.search import NoteSearch, ArticleNoteSearch
from notes.storage.search.elastic import ElasticNoteSearch, ElasticArticleNoteSearch, DirectorySearch


def get_note_search(request: Request) -> NoteSearch:
    return request["note_search"]


def get_article_note_search(request: Request) -> ArticleNoteSearch:
    return request["article_note_search"]


def get_general_search(request: Request) -> DirectorySearch:
    return request["directory_search"]


def add_search(connection: es.Elasticsearch):
    @middleware
    async def add_search_middleware(request: Request, handler):
        request["note_search"] = ElasticNoteSearch(connection)
        request["article_note_search"] = ElasticArticleNoteSearch(connection)
        request["directory_search"] = DirectorySearch(connection)
        return await handler(request)

    return add_search_middleware
