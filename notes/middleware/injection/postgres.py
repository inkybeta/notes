import asyncpg.pool
from aiohttp.web import middleware, Request, Application

from notes.storage.directory.postgres import PostgresDirectoryStore
from notes.storage.notes.postgres import PostgresNoteStore, PostgresArticleStore
from notes.storage.authentication.postgres import PostgresAuthenticationStorage
from notes.storage.notes.store import NoteStore, ArticleNoteStore
from notes.storage.directory import DirectoryStore


def get_note_store(request: Request) -> NoteStore:
    return request["note_store"]


def get_article_note_store(request: Request) -> ArticleNoteStore:
    return request["article_note_store"]


def get_directory_store(request: Request) -> DirectoryStore:
    return request["directory_store"]


def add_postgres_storage(app: Application, pool: asyncpg.pool.Pool):
    app["pool"] = pool

    @middleware
    async def add_storage(request: Request, handler):
        conn = await request.app["pool"].acquire()
        request["connection"] = conn
        request["note_store"] = PostgresNoteStore(conn)
        request["article_note_store"] = PostgresArticleStore(conn)
        request["directory_store"] = PostgresDirectoryStore(conn)
        request["authentication_storage"] = PostgresAuthenticationStorage(conn)
        return await handler(request)

    return add_storage


@middleware
async def cleanup_connection(request: Request, handler):
    try:
        resp = await handler(request)
        if "connection" in request and not request["connection"].is_closed():
            await request["connection"].close()
        return resp
    except BaseException as err:
        if "connection" in request and not request["connection"].is_closed():
            await request["connection"].close()
        raise err
