from aiohttp.web import Request, middleware
import firebase_admin as admin


def add_firebase(app: admin.App):
    @middleware
    async def add_firebase_middleware(request, handler):
        request["firebase_app"] = app
        return await handler(request)
    return add_firebase_middleware


def get_firebase_application(request: Request) -> admin.App:
    return request["firebase_app"]
