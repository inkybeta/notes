import json
import uuid
from datetime import datetime

from aiohttp.web import middleware, Request, json_response
from aiohttp.web_exceptions import HTTPError

from notes.logging import error_msg_format
from notes.middleware.request_idenfitication import get_request_id
from notes.models.errors import StandardError
from notes.models.response import StandardResponse
from . import LOGGER
import traceback
import sys


def make_dict(obj):
    if obj is None:
        return obj
    if type(obj) is bool or type(obj) is int or type(obj) is float or type(obj) is str:
        return obj
    elif type(obj) is datetime:
        obj: datetime = obj
        return obj.isoformat()
    elif type(obj) is list:
        return [make_dict(i) for i in obj]
    elif type(obj) is tuple:
        return {i: make_dict(value) for i, value in enumerate(list(obj))}
    elif type(obj) is dict:
        return {key: make_dict(value) for key, value in obj.items()}
    elif isinstance(obj, uuid.UUID):
        return obj.hex
    elif isinstance(obj, object):
        return {key: make_dict(value) for key, value in obj.__dict__.items()}
    else:
        return obj


def jsonify(obj):
    if isinstance(obj, StandardError):
        obj: StandardError = obj
        return json.dumps({
            "success": obj.success,
            "code": obj.code,
            "error": obj.error,
            "reason": obj.reason,
            "friendly": obj.friendly,
            "payload": obj.payload,
            "warnings": []
        })
    elif isinstance(obj, StandardResponse):
        obj: StandardResponse = obj
        return json.dumps({
            "success": obj.success,
            "code": obj.code,
            "error": obj.error,
            "reason": obj.reason,
            "friendly": obj.friendly,
            "payload": make_dict(obj.payload),
            "warnings": []
        })
    else:
        wrapped = make_dict(StandardResponse(True, 200, "", "", "", make_dict(obj)))
        return json.dumps(wrapped)


# noinspection PyBroadException,PyUnresolvedReferences
@middleware
async def handle_errors(request: Request, handler):
    try:
        result = await handler(request)
        return result
    except StandardError as err:
        headers = None
        if "headers" in vars(err):
            headers = err.headers
        response = json_response(err, dumps=jsonify, status=err.code, headers=headers)
        return response
    except HTTPError as err:
        return err
    except BaseException as err:
        traceback.print_exc(file=sys.stdout)
        LOGGER.error(error_msg_format(get_request_id(request), err))
        headers = None
        if "headers" in dir(err):
            headers = err.headers
        return json_response({
            "success": False,
            "code": 503,
            "error": "server/generic",
            "reason": "There was an unknown error on the server.",
            "friendly": "Something happened on the server. Check again later",
            "payload": None,
            "warnings": []
        }, headers=headers, status=503)


@middleware
async def standardize(request: Request, handler):
    result = await handler(request)
    return json_response(result, dumps=jsonify)
