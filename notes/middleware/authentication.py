from aiohttp.web import middleware, Request
from notes.models import AuthenticatedUser
from notes.models.errors import StandardError, InvalidAuthorizationTokenError
from notes.middleware.injection import get_firebase_application
import firebase_admin.auth as auth
import uuid


class NotAuthorizedError(StandardError):
    def __init__(self):
        StandardError.__init__(self, 401, "You are not authorized to access this resource.", "You aren't logged in. Try"
                                                                                             " logging in again.")


def get_authenticated_user(request) -> AuthenticatedUser:
    return request["authentication"]


@middleware
async def authenticate(request: Request, handler):
    if "Authorization" not in request.headers:
        request["authentication"] = AuthenticatedUser(False)
        return await handler(request)
    authorization_header = str(request.headers["Authorization"]).split(' ', maxsplit=1)
    if authorization_header[0] == "Firebase":
        try:
            token = auth.verify_id_token(authorization_header[1], get_firebase_application(request), True)
            request["authentication"] = AuthenticatedUser(True, uuid.UUID(token["uid"]), token["email_verified"])
        except auth.AuthError as e:
            request["authentication"] = AuthenticatedUser(False)
        except ValueError:
            raise InvalidAuthorizationTokenError()
    else:
        raise NotAuthorizedError()
    return await handler(request)


def authorize(verified=True):
    def authorize_internal(method):
        def authorize_wrapper(request: Request):
            user = get_authenticated_user(request)
            if not user.authenticated:
                raise NotAuthorizedError()
            if verified and not user.verified:
                raise NotAuthorizedError()
            return method(request)

        return authorize_wrapper

    return authorize_internal
