import asyncio

import asyncpg
from aiohttp import web
from notes.middleware.cors import enable_cross_origin
from notes.middleware.authentication import authenticate
from notes.middleware.injection import add_postgres_storage, cleanup_connection, add_search, add_firebase
from notes.middleware.standardization import standardize, handle_errors
from notes.middleware.request_idenfitication import add_request_tagging
from notes.api import routes
from notes.logging import configure

import configparser
import elasticsearch as es
import firebase_admin as admin


async def generate(configuration: configparser.ConfigParser) -> web.Application:
    host = configuration["Database"].get("Host")
    database = configuration["Database"].get("Database")
    user = configuration["Database"].get("Username")
    password = configuration["Database"].get("Password")
    pool = await asyncpg.create_pool(host=host, database=database, user=user,
                                     password=password if password != "" else None)

    elastic_search = es.Elasticsearch(hosts=[configuration["Search"].get("Host")],
                                      verify_certs=configuration.getboolean("Search", "EnforceSecure"))

    configure("logging.cfg")

    firebase = admin.initialize_app(admin.credentials.Certificate("./firebase.config.json"))

    app = web.Application()
    middleware_sequence = [add_request_tagging,
                           handle_errors,
                           cleanup_connection,
                           add_firebase(firebase),
                           enable_cross_origin("*"),
                           add_postgres_storage(app, pool),
                           authenticate,
                           add_search(elastic_search),
                           standardize]
    app.middlewares.extend(middleware_sequence)
    app.add_routes(routes)
    return app


if __name__ == "__main__":
    parser = configparser.ConfigParser(allow_no_value=True)
    parser.read_file(open("configuration.cfg", "r"))
    loop = asyncio.get_event_loop()
    web_app = loop.run_until_complete(generate(parser))
    web.run_app(web_app, port=int(parser["Server"].get("Port")) if parser["Server"].get("Port") else 8080)
