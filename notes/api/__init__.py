from aiohttp.web import RouteTableDef

routes = RouteTableDef()

import notes.api.note
import notes.api.directory
import notes.api.user.auth
import notes.api.search
