import datetime

from aiohttp.web import Request

from notes.middleware.authentication import get_authenticated_user
from notes.middleware.injection import get_general_search
from notes.models.errors import ModelTypeError
from notes.tools import parse_datetime
from notes.tools.http import extract


async def search_all(request: Request, **kwargs):
    title: str = extract(kwargs, "title", str, required=False)

    content: str = extract(kwargs, "content", str, required=False)
    tags: [str] = extract(kwargs, "tags", list, "str[]", required=False)
    authors: [str] = extract(kwargs, "authors", list, "str[]", required=False)
    url: str = extract(kwargs, "url", str, required=False)

    try:
        start_date: datetime.datetime = parse_datetime(kwargs["start_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("start_date", "datetime")
    try:
        end_date: datetime.datetime = parse_datetime(kwargs["end_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("end_date", "datetime")

    path = extract(kwargs, "path", str, required=False)

    start: int = extract(kwargs, "start", int, "int", required=False)
    count: int = extract(kwargs, "count", int, "int", required=False)

    if count and count > 50:
        count = 50
    elif not count:
        count = 30

    search = get_general_search(request)
    auth = get_authenticated_user(request)

    result = await search.find(user_id=auth.user_id,
                               title=title,
                               text=content,
                               path=path,
                               tags=tags,
                               authors=authors,
                               url=url,
                               start_date=start_date,
                               end_date=end_date,
                               start=start,
                               count=count)
    return result
