from notes.middleware.authentication import authorize
from notes.models.errors import BodyMissingError
from .. import routes
from aiohttp.web import Request
from .gateway import search_all


@routes.post("/api/search")
@authorize()
async def search(request: Request):
    if not request.can_read_body:
        raise BodyMissingError()
    return await search_all(request, **(await request.json()))
