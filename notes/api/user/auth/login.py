from . import routes
import firebase_admin.auth as auth
from aiohttp.web import Request
from notes.tools.http import extract
from notes.middleware.injection import get_firebase_application


@routes.put("/api/user/")
async def login(request: Request):
    fb_app = get_firebase_application(request)
    email = extract(request, "email", str)
    password = extract(request, "password", str)
    user = auth.get_user_by_email(email, fb_app)
    auth.create_custom_token(user.uid, user.custom_claims)
