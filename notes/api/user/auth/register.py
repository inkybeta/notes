from .. import routes
from aiohttp.web import Request
from .gateway import register_user


@routes.post("/api/user")
async def register(request: Request):
    body = await request.json()
    return await register_user(request, **body)
