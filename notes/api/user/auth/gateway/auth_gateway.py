from notes.tools.http import extract, parse_email
from notes.models.auth import User, AuthenticationType
from notes.models.errors import ModelInputError
from notes.middleware.injection import get_authentication_storage, get_firebase_application
from notes.middleware.authentication import get_authenticated_user
import firebase_admin.auth as auth
import uuid


async def register_user(request, **body):
    email = parse_email(extract(body, "email", str))
    first_name = extract(body, "first_name", str)
    last_name = extract(body, "last_name", str)
    middle_name = extract(body, "middle_name", str, default=None, required=False)
    password = extract(body, "password", str)
    authentication_type: str = extract(body, "authentication_type", str)

    store = get_authentication_storage(request)
    firebase = get_firebase_application(request)
    if authentication_type.lower() == "firebase":
        user_id = uuid.uuid4()
        user = User(user_id=user_id, email=email, first_name=first_name, last_name=last_name,
                    authentication_type=AuthenticationType.FIREBASE, middle_name=middle_name)
        await store.register(user)
        try:
            auth.create_user(uid=user_id.hex, email=user.email, password=password, app=firebase)
        except ValueError as err:
            raise ModelInputError(str(err))
        await store.commit()
        return user_id
    raise NotImplementedError


async def get_user(request, **body):
    user = get_authenticated_user(request)
    store = get_authentication_storage(request)
    return await store.get_user(user.user_id)
