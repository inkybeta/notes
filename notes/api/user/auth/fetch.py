from .. import routes
from aiohttp.web import Request
from notes.middleware.authentication import authorize
from .gateway import get_user as gu


@authorize()
@routes.get("/api/user")
async def get_user(request: Request):
    return await gu(request)
