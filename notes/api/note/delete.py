"""
This module handles deleting notes from the server. It principally follows the rules of the other endpoints as well in
requiring authentication and the type of note to be specified.
"""
from aiohttp.web import Request

from notes.api import routes
from notes.api.note.gateway.article_note_gateway import delete_article_note
from notes.api.note.gateway.note_gateway import delete_note
from notes.middleware.authentication import authorize
from notes.models.errors import MissingParameterError


@authorize()
@routes.delete("/api/note")
async def delete(request: Request):
    """
    Handles deleting notes from the database. The client should be authenticated before doing this operation
    :param request: the incoming request
        The request should have several components:
            -type (str): the type of note to delete (note, article_note)
            -note_id (str): the id of the note given as a hexadecimal string
    :return: a boolean determining if the operation succeeded
    """
    body = await request.json()
    if "type" not in body:
        raise MissingParameterError("type")
    if body["type"] == "note":
        return await delete_note(request, body)
    elif body["type"] == "article_note":
        return await delete_article_note(request, body)
    else:
        raise MissingParameterError("type")
