from notes.middleware.authentication import authorize
from aiohttp.web import Request

from notes.api import routes

from notes.api.note.gateway.article_note_gateway import (get_article_note, list_article_notes,
                                                         get_partial_article_note_updates,
                                                         get_directory_article_note_items, get_article_note_path)
from notes.api.note.gateway.note_gateway import (get_note, list_notes, get_partial_note_updates,
                                                 get_directory_note_items, get_note_path)

from typing import Dict, Any
from notes.models.errors import MissingParameterError


@authorize()
@routes.get("/api/note")
async def get(request: Request):
    """
    Handles a request to fetch a note or a list of notes depending on the given input. This method requires
    authentication from the client.
    :param request: the incoming request
        The request should have one of several components depending on the requested operation. This will be labelled
        in different sections:
            -type (str): the type of note (note, article_note)
            To select a specific note:
                -note_id (str): the id of the note to fetch
            To list the notes held by a user:
                -limit (int)[optional with default 15]: the number of note ids to get
                -skip (int)[optional with default 0]: the number of notes to skip
                Note: this method only returns a fraction of the total notes available and selects the [limit] number
                of notes or less from a list ordered chronologically. More recent notes will be selected first.
            To query notes based on the parameters:
                -limit (int)[optional with default 15]: the number of note ids to get
                -skip (int)[optional with default 0]: the number of notes to skip
                -<parameters> (various)[optional]: a list of parameters to constrain the search by.
                Note: the method is similar to the
    :return: a list of notes fitting the query
    """
    parameters: Dict[str, Any] = request.query
    if request.can_read_body:
        parameters = await request.json()
    if "type" not in parameters:
        return MissingParameterError("type")
    if "note_id" in parameters:
        if parameters["type"] == "note":
            return await get_note(request, parameters)
        elif parameters["type"] == "article_note":
            return await get_article_note(request, parameters)
        else:
            return MissingParameterError("note")
    if "directory_id" in parameters or "directory" in parameters:
        if parameters["type"] == "note":
            return await get_directory_note_items(request, **parameters)
        elif parameters["type"] == "article_note":
            return await get_directory_article_note_items(request, **parameters)
    else:
        if parameters["type"] == "note":
            return await list_notes(request, **parameters)
        elif parameters["type"] == "article_note":
            return await list_article_notes(request, **parameters)


@routes.get("/api/note/partials")
@authorize()
async def get_partials(request: Request):
    """
    Handles a request to fetch the partial updates of a given note.
    :param request:
    :return:
    """
    body = await request.json()
    if body["type"] == "note":
        return await get_partial_note_updates(request, body)
    elif body["type"] == "article_note":
        return await get_partial_article_note_updates(request, body)
    else:
        return MissingParameterError("type")


@routes.get("/api/note/path")
@authorize()
async def get_path(request: Request):
    """

    :param request:
    :return:
    """
    body = request.query
    if body["type"] == "note":
        return await get_note_path(request, **body)
    elif body["type"] == "article_note":
        return await get_article_note_path(request, **body)
    else:
        return MissingParameterError("type")
