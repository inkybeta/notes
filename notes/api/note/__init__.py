"""
This module represents a gateway to controlling notes and their related CRUD operations. This includes the various types
of notes that will be used. Note that all responses will utilize the StandardResponse object that contains a boolean,
string, and payload. The description of the return object of each method matches the payload portion of that object.
"""
from . import delete, fetch, save, search, modify
