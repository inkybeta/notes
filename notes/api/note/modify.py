from notes.middleware.authentication import authorize
from .. import routes
from aiohttp.web import Request
from notes.tools.http.model import extract
from notes.models.errors import MissingParameterError
from .gateway.note_gateway import move_note as mvn
from .gateway.article_note_gateway import move_article_note


@authorize()
@routes.put("/api/note/directory")
async def move_note(request: Request):
    body = await request.json()
    note_type = extract(body, "type", str)
    if not note_type:
        raise MissingParameterError("type")
    if note_type == "note":
        return await mvn(request, **body)
    elif note_type == "article_note":
        return await move_article_note(request, **body)
