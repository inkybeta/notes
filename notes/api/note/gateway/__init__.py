"""
The gateways control access to the actual stores of information and attempts to validate the model
"""
from .article_note_gateway import get_directory_article_notes, get_directory_article_note_items
from .note_gateway import get_directory_notes, get_directory_note_items
