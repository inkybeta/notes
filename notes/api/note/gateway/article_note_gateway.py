import datetime
import uuid

from aiohttp.web import Request

from notes.api.directory.gateway import get_directory_id
from notes.middleware.authentication import get_authenticated_user
from notes.models import AuthenticatedUser, Edit, Note, NoteUpdate, ArticleNote
from notes.models.errors import ModelTypeError
from notes.storage.notes.store import ArticleNoteStore
from notes.tools.http.model import extract, parse_uuid
from notes.tools.time import parse_datetime

from notes.middleware.injection import get_article_note_store, get_article_note_search
from typing import List


async def add_article_note(request: Request, body=None):
    store: ArticleNoteStore = get_article_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    note_id = uuid.uuid4()
    
    unprepared_timestamp = extract(body, "timestamp", str, type_name="timestamp", required=False)
    timestamp = parse_datetime(unprepared_timestamp) if not unprepared_timestamp else datetime.datetime.now()
    unprepared_published = extract(body, "published", str, type_name="timestamp", required=False)
    published = parse_datetime(unprepared_published)

    directory_id = await get_directory_id(request, **body)

    note = ArticleNote(note_id=note_id,
                       user_id=auth.user_id,
                       title=extract(body, "title", str),
                       summary=extract(body, "content", str, required=False, default=""),
                       timestamp=timestamp,
                       created=timestamp,
                       tags=extract(body, "tags", List[str], required=False),
                       url=body["url"],
                       published=published,
                       authors=body["authors"],
                       directory_id=directory_id)
    await store.add_article_notes(note)
    await store.commit()
    return {
        "note_id": str(note_id.hex)
    }


async def get_article_note(request: Request, body=None):
    store: ArticleNoteStore = get_article_note_store(request)
    auth: AuthenticatedUser = request["authentication"]
    if not body:
        body = await request.json()
    note = await store.get_article_note(auth.user_id, body["note_id"])
    return note


async def update_article_note(request: Request, body=None):
    store: ArticleNoteStore = request["article_note_store"]
    auth: AuthenticatedUser = request["authentication"]
    if not body:
        body = await request.json()
    note: Note = store.get_article_note(auth.user_id, body["note_id"])
    edit_id = uuid.uuid4()
    edits = []
    for edit in body["text_updates"]:
        if edit["action"] == Edit.INSERTION or edit["action"] == Edit.REPLACEMENT:
            edits.append(
                Edit(edit["action"], edit["index"], text=edit["text"]))
        elif edit["action"] == Edit.DELETION:
            edits.append(
                Edit(Edit.DELETION, edit["index"], text=edit["text"]))
    note_information = await store.get_note_information(auth.user_id, note.note_id, keep_limit=15)
    if note_information.commit_count >= 15:
        await store.delete_previous_updates(auth.user_id, note.note_id, note_information.earliest_edit_time)
    await note.apply_update(NoteUpdate(
        auth.user_id, body["note_id"], edit_id, body["timestamp"], title_update=body["title_update"],
        text_updates=edits, url=body["url"], authors=body["authors"]))
    await store.commit()
    return edit_id


async def delete_article_note(request: Request, body=None):
    store: ArticleNoteStore = get_article_note_store(request)
    auth: AuthenticatedUser = request["authentication"]
    if not body:
        body = await request.json()
    await store.remove_article_note(auth.user_id, uuid.UUID(body["note_id"]))
    await store.commit()
    return True


async def list_article_notes(request: Request, **kwargs):
    try:
        limit: int = int(kwargs["limit"]) if "limit" in kwargs else 15
    except ValueError:
        return ModelTypeError("limit", "int")
    try:
        skip: int = int(kwargs["skip"]) if "skip" in kwargs else 0
    except ValueError:
        return ModelTypeError("skip", "int")
    store: ArticleNoteStore = get_article_note_store(request)
    auth: AuthenticatedUser = request["authentication"]
    return await store.list_article_notes(auth.user_id, limit, skip)


async def get_partial_article_note_updates(request: Request, body=None):
    store: ArticleNoteStore = get_article_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    return await store.get_partial_article_updates(auth.user_id, body["note_id"])


async def search_article_notes(request: Request, **kwargs):
    search = get_article_note_search(request)
    auth: AuthenticatedUser = get_authenticated_user(request)

    title: str = kwargs["title"] if "title" in kwargs else None
    if title and type(title) is not str:
        raise ModelTypeError("title", "str")

    content: str = extract(kwargs, "content", str)
    tags: List[str] = extract(kwargs, "tags", list, "str[]")
    authors: List[str] = extract(kwargs, "authors", list, "str[]")
    url: str = extract(kwargs, "url", str)

    try:
        start_date: datetime.datetime = parse_datetime(kwargs["start_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("start_date", "datetime")
    try:
        end_date: datetime.datetime = parse_datetime(kwargs["end_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("end_date", "datetime")

    path = extract(kwargs, "content", str)

    start: int = extract(kwargs, "start", int, "int")
    count: int = extract(kwargs, "count", int, "int")

    if count and count > 50:
        count = 50
    elif not count:
        count = 30

    result = await search.find_article_notes(user_id=auth.user_id,
                                             title=title,
                                             text=content,
                                             path=path,
                                             tags=tags,
                                             authors=authors,
                                             url=url,
                                             start_date=start_date,
                                             end_date=end_date,
                                             start=start,
                                             count=count)
    return result


async def move_article_note(request: Request, **kwargs):
    note_id = parse_uuid(extract(kwargs, "note_id", str))
    directory_id = await get_directory_id(request, kwargs=kwargs)
    store = get_article_note_store(request)
    user = get_authenticated_user(request)
    result = await store.move_article_note(user.user_id, note_id, directory_id)
    await store.commit()
    return result


async def get_article_note_path(request: Request, **kwargs):
    note_id: uuid.UUID = parse_uuid(extract(kwargs, "note_id", str))
    auth = get_authenticated_user(request)
    store = get_article_note_store(request)
    return await store.get_path(auth.user_id, note_id)


async def get_directory_article_notes(request: Request, **kwargs):
    store = get_article_note_store(request)
    user = get_authenticated_user(request)

    directory_id = await get_directory_id(request, **kwargs)
    return await store.get_directory_article_notes(user.user_id, directory_id)


async def get_directory_article_note_items(request: Request, **kwargs):
    store = get_article_note_store(request)
    user = get_authenticated_user(request)
    directory_id = await get_directory_id(request, **kwargs)
    return await store.get_directory_article_note_items(user.user_id, directory_id)
