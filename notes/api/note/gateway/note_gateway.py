import uuid

from aiohttp.web import Request

import notes.api.directory.gateway
from notes.models import AuthenticatedUser, Edit, Note, NoteUpdate
from notes.storage.notes.store import NoteStore
from notes.models.errors import ModelTypeError
from notes.middleware.injection import get_note_search, get_note_store
from notes.middleware.authentication import get_authenticated_user
from notes.storage.search import NoteSearch
from notes.tools.time import parse_datetime
from notes.tools.http.model import extract, parse_uuid
from notes.api.directory.gateway import get_directory_id

import datetime

from typing import List


async def add_note(request: Request, **body):
    store: NoteStore = get_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    search: NoteSearch = get_note_search(request)
    if not body:
        body = await request.json()
    note_id = uuid.uuid4()
    timestamp = datetime.datetime.strptime(body["timestamp"], "%Y-%m-%dT%H:%M:%S.%fZ")
    directory_id = await get_directory_id(request, **body)
    note = Note(note_id=note_id,
                user_id=auth.user_id,
                title=body["title"],
                content=body["content"],
                timestamp=timestamp,
                created=timestamp,
                tags=body["tags"],
                directory_id=directory_id)
    await store.add_notes(auth.user_id, note)
    if not await search.add_note(auth.user_id, note_id, note):
        await store.rollback()
        return None
    await store.commit()
    return note_id


async def get_note(request: Request, body):
    if not body["note_id"] or type(body["note_id"]) is not str:
        return ModelTypeError("note_id")
    store: NoteStore = get_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    note = await store.get_note(auth.user_id, body["note_id"])
    return note


async def update_note(request: Request, body=None):
    store: NoteStore = get_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    search: NoteSearch = get_note_search(request)
    if not body:
        body = await request.json()
    note: Note = await store.get_note(auth.user_id, parse_uuid(body["note_id"]))
    edit_id = uuid.uuid4()
    edits = []
    for edit in body["text_updates"]:
        if "action" not in edit:
            raise ModelTypeError("edit")
        if edit["action"] == Edit.INSERTION or edit["action"] == Edit.REPLACEMENT:
            edits.append(
                Edit(edit["action"], edit["index"], text=edit["text"]))
        elif edit["action"] == Edit.DELETION:
            edits.append(
                Edit(Edit.DELETION, edit["index"], text=edit["text"]))
    update = NoteUpdate(
        auth.user_id, parse_uuid(body["note_id"]), edit_id, parse_datetime(body["timestamp"]),
        title_update=body["title_update"],
        text_updates=edits)
    note.apply_update(update)

    # Check there isn't an overflow of partial updates kept in the database
    await store.update_note(note, update)
    if not await search.update_note(auth.user_id, note.note_id, note=note):
        await store.rollback()
        return None
    note_information = await store.get_note_information(auth.user_id, note.note_id, keep_limit=15)
    if note_information.commit_count >= 15:
        await store.delete_previous_updates(auth.user_id, note.note_id, note_information.earliest_edit_time)
    await store.commit()
    return edit_id.hex


async def delete_note(request: Request, body=None):
    store: NoteStore = get_note_store(request)
    auth: AuthenticatedUser = get_authenticated_user(request)
    search: NoteSearch = get_note_search(request)
    note_id = parse_uuid(body["note_id"])
    await store.remove_note(auth.user_id, note_id)

    if not await search.delete_note(auth.user_id, note_id):
        await store.rollback()
        return None
    await store.commit()
    return True


async def get_partial_note_updates(request: Request, body=None):
    store: NoteStore = request["note_store"]
    auth: AuthenticatedUser = get_authenticated_user(request)
    if not body:
        body = await request.json()
    partials = store.get_partial_updates(auth.user_id, body["note_id"])
    return partials


async def list_notes(request: Request, **kwargs):
    limit: int = kwargs["limit"] if "limit" in kwargs else 15
    skip: int = kwargs["skip"] if "skip" in kwargs else 0
    if type(limit) is not int:
        return ModelTypeError("limit", "int")
    if type(skip) is not int:
        return ModelTypeError("skip", "int")
    store: NoteStore = request["note_store"]
    auth: AuthenticatedUser = get_authenticated_user(request)
    result = await store.list_notes(auth.user_id)
    return [i.hex for i in result]


async def get_note_path(request: Request, **kwargs):
    note_id: uuid.UUID = parse_uuid(extract(kwargs, "note_id", str))
    auth = get_authenticated_user(request)
    store = get_note_store(request)
    return await store.get_path(auth.user_id, note_id)


async def search_notes(request: Request, **kwargs):
    search = get_note_search(request)
    auth: AuthenticatedUser = get_authenticated_user(request)

    title: str = kwargs["title"] if "title" in kwargs else None
    if title and type(title) is not str:
        raise ModelTypeError("title", "str")

    content: str = extract(kwargs, "content", str, "str")
    tags: List[str] = extract(kwargs, "tags", list, "str[]")

    try:
        start_date: datetime.datetime = parse_datetime(kwargs["start_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("start_date", "datetime")
    try:
        end_date: datetime.datetime = parse_datetime(kwargs["end_date"]) if "start_date" in kwargs else None
    except ValueError:
        raise ModelTypeError("end_date", "datetime")

    path = extract(kwargs, "content", str)

    start: int = extract(kwargs, "start", int, "int")
    count: int = extract(kwargs, "count", int, "int")

    if count and count > 50:
        count = 50
    elif not count:
        count = 30

    result = await search.find_notes(user_id=auth.user_id,
                                     title=title,
                                     text=content,
                                     path=path,
                                     tags=tags,
                                     start_date=start_date,
                                     end_date=end_date,
                                     start=start,
                                     count=count)
    return result


async def move_note(request: Request, **kwargs):
    note_id = parse_uuid(extract(kwargs, "note_id", str))
    directory_id = await notes.api.directory.gateway.get_directory_id(request, kwargs=kwargs)
    store = get_note_store(request)
    user = get_authenticated_user(request)
    result = await store.move_note(user.user_id, note_id, directory_id)
    await store.commit()
    return result


async def get_directory_notes(request: Request, **kwargs):
    directory_id = await notes.api.directory.gateway.get_directory_id(request, **kwargs)
    user = get_authenticated_user(request)
    store = get_note_store(request)
    return await store.get_directory_notes(user.user_id, directory_id)


async def get_directory_note_items(request: Request, **kwargs):
    directory_id = await notes.api.directory.gateway.get_directory_id(request, **kwargs)
    user = get_authenticated_user(request)
    store = get_note_store(request)
    return store.get_directory_note_items(user.user_id, directory_id)
