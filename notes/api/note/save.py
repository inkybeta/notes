import aiohttp.web as web
from aiohttp.web import Request

from notes.api import routes

from notes.api.note.gateway.article_note_gateway import (add_article_note, update_article_note)
from notes.api.note.gateway.note_gateway import (add_note, update_note)
from notes.middleware.authentication import authorize
from notes.models.errors import MissingParameterError


@routes.post("/api/note")
@authorize()
async def add(request: Request):
    """
    Handles operations to add notes to the database. The client should be authenticated
    before doing this operation.
    :param request: the incoming request
        The request body should have several components:
            -type (str): the type of note to add (note, article_note)
            -title (str): all notes should have a title attached that is no more than 100 characters long
            -content (str): the content of the note
            -timestamp (datetime): the date and time the note was created in UTC time without a timezone
            -tags (str[]): a list of tags to associate with the note
            -url [article_note only](str): the url of the article
            -authors [article_note only](str[]): a list of authors to add
    """
    body = await request.json()
    if "type" not in body:
        raise MissingParameterError("type")
    if body["type"] == "note":
        return await add_note(request, **body)
    elif body["type"] == "article_note":
        return await add_article_note(request, body)
    else:
        return await web.json_response("error")


@routes.patch("/api/note")
@authorize()
async def update(request: Request):
    """
    Handles updating notes in the database. The client should be authenticated before doing this operation
    :param request:
    :return: a string representing the hexadecimal representation of the edit id.
    """
    body = await request.json()
    if "type" not in body:
        raise MissingParameterError("type")
    if body["type"] == "note":
        return await update_note(request, body)
    elif body["type"] == "article_note":
        return await update_article_note(request, body)
    else:
        raise MissingParameterError("type")
