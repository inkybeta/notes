from notes.middleware.authentication import authorize
from notes.models.errors import MissingParameterError
from .. import routes
from .gateway.note_gateway import search_notes
from .gateway.article_note_gateway import search_article_notes
from aiohttp.web import Request


@authorize()
@routes.post("/api/note/search")
async def search(request: Request):
    body = await request.json()
    if "type" not in body:
        return MissingParameterError("type")
    if body["type"] == "note":
        return await search_notes(request, **body)
    elif body["type"] == "article_note":
        return await search_article_notes(request, title=body["title"], content=body["content"], tags=body["tags"],
                                          authors=body["authors"], url=body["url"], start_date=body["start_date"],
                                          end_date=body["end_date"], start=body["start"], count=body["count"])
    else:
        return "error"
