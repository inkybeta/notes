from .. import routes
from .gateway.directory_gateway import delete_directory
from notes.middleware.authentication import authorize
from notes.models.errors import BodyMissingError
from aiohttp.web import Request


@authorize()
@routes.delete("/api/directory")
async def remove_directory(request: Request):
    if not request.can_read_body:
        raise BodyMissingError()
    body = await request.json()
    return await delete_directory(request, **body)
