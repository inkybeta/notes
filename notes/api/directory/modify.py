from .. import routes
from notes.middleware.authentication import authorize
from aiohttp.web import Request
from notes.models.errors import BodyMissingError
from .gateway.directory_gateway import move_directory, rename_directory


@authorize()
@routes.patch("/api/directory")
async def modify_directory(request: Request):
    if not request.can_read_body:
        raise BodyMissingError()
    body = await request.json()
    if "destination_id" in body:
        # We need to move the directory
        return await move_directory(request, **body)
    else:
        return await rename_directory(request, **body)
