from .. import routes
from .gateway import add_directory as ad
from notes.middleware.authentication import authorize
from notes.models.errors import BodyMissingError
from aiohttp.web import Request


@authorize()
@routes.post("/api/directory")
async def add_directory(request: Request):
    if not request.can_read_body:
        raise BodyMissingError()
    body = await request.json()
    return await ad(request, **body)
