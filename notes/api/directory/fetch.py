from aiohttp.web import Request

from notes.middleware.authentication import authorize
from .gateway.directory_gateway import get_directory as gd
from .. import routes


@routes.get("/api/directory")
@authorize()
async def get_directory(request: Request):
    body = request.query
    return await gd(request, **body)
