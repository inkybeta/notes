from aiohttp.web import Request

from notes.middleware.authentication import authorize
from notes.models.errors import BodyMissingError
from .gateway import get_directory_items as get_directory_items_gate
from .. import routes


@authorize()
@routes.post("/api/directory/children")
async def get_children(request: Request):
    if request.body_exists:
        body = await request.json()
    else:
        raise BodyMissingError()
    return await get_directory_items_gate(request, **body)
