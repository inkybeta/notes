import logging
import uuid
from typing import Optional

from aiohttp.web_request import Request

from notes.logging import GATEWAY_LOGGER
from notes.models.errors import ModelTypeError

LOGGER = logging.getLogger(GATEWAY_LOGGER)


async def get_directory_id(request: Request, **kwargs) -> Optional[uuid.UUID]:
    store = get_directory_store(request)
    user_id = get_authenticated_user(request).user_id
    raw_directory_id = extract(kwargs, "directory_id", str, required=False)

    # Check if either parameter has been specified. Otherwise, just return the None value
    if "directory" not in kwargs and "directory_id" not in kwargs:
        return None

    if raw_directory_id:
        LOGGER.info(request_msg_format(get_request_id(request), "get_directory_id", raw_directory_id=raw_directory_id))
        directory_id = parse_uuid(raw_directory_id)
    else:
        directory_id = None

    if directory_id or "directory" not in kwargs:
        LOGGER.info(response_msg_format(get_request_id(request), "get_directory_id", directory_id=directory_id))
        return directory_id

    LOGGER.debug(request_msg_format(get_request_id(request), "get_directory_id", directory_id_found=False))
    directory = kwargs["directory"] if "directory" in kwargs else None
    if directory and type(directory) is not dict:
        raise ModelTypeError("directory", "dict")

    path = extract(kwargs["directory"], "path", str)
    name = extract(kwargs["directory"], "name", str)
    LOGGER.info(request_msg_format(get_request_id(request), "get_directory_id", path=path, name=name))
    if not path:
        return None

    directory_id = await store.get_directory_id(user_id=user_id, path=path, name=name)
    LOGGER.debug(response_msg_format(get_request_id(request), "get_directory_id", directory_id=directory_id))
    return directory_id


from .directory_gateway import *
