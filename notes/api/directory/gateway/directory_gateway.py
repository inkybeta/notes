import uuid
from typing import Optional

from aiohttp.web_request import Request

from notes.middleware.authentication import get_authenticated_user
from notes.middleware.injection import get_directory_store, get_note_store, get_article_note_store
from notes.models.directory import Directory
from notes.models.errors import ModelTypeError
from notes.storage.directory.store import InvalidDirectory
from notes.tools.http.model import extract, parse_uuid

from notes.api.directory.gateway import get_directory_id
from notes.middleware.request_idenfitication import get_request_id
import logging
from notes.logging import GATEWAY_LOGGER, request_msg_format, response_msg_format

LOGGER = logging.getLogger(GATEWAY_LOGGER)


async def get_directory(request: Request, **kwargs) -> Optional[Directory]:
    store = get_directory_store(request)
    user = get_authenticated_user(request)
    directory_id = await get_directory_id(request, **kwargs)

    LOGGER.info(request_msg_format(get_request_id(request), "get_directory", user_id=user.user_id,
                                   directory_id=directory_id.hex if directory_id else None))
    v = await store.get_directory(user.user_id, directory_id)
    if not v:
        return v
    await store.commit()
    v.children = await get_directory_items(request, **kwargs)
    LOGGER.info(response_msg_format(get_request_id(request), "get_directory"))

    return v


async def delete_directory(request: Request, **kwargs):
    directory_id = parse_uuid(extract(kwargs, "directory_id", str))
    user = get_authenticated_user(request)
    directory_store = get_directory_store(request)

    LOGGER.info(request_msg_format(get_request_id(request), "delete_directory", user_id=user.user_id,
                                   directory_id=directory_id.hex))
    deleted = await directory_store.remove_directory(user.user_id, directory_id)
    LOGGER.info(response_msg_format(get_request_id(request), "delete_directory"))
    await directory_store.commit()
    return deleted


async def add_directory(request: Request, **kwargs):
    name = extract(kwargs, "name", str)
    path = extract(kwargs, "path", str)

    raw_parent_id = extract(kwargs, "parent_id", str, required=False)

    user = get_authenticated_user(request)
    directory_store = get_directory_store(request)

    # Check if either parameter has been specified. Otherwise, just return the None value
    if "parent" not in kwargs and "parent_id" not in kwargs:
        parent_id = None
    elif "parent_id" in kwargs:
        LOGGER.info(request_msg_format(get_request_id(request), "add_directory", raw_parent_id=raw_parent_id))
        parent_id = parse_uuid(raw_parent_id)
    else:
        LOGGER.debug(request_msg_format(get_request_id(request), "add_directory", directory_id_found=False))
        directory = kwargs["parent"] if "parent" in kwargs else None
        if directory and type(directory) is not dict:
            raise ModelTypeError("parent", "dict")

        path = extract(kwargs["parent"], "path", str)
        name = extract(kwargs["parent"], "path", str)
        LOGGER.info(request_msg_format(get_request_id(request), "add_directory", path=path, gateway_method=name))
        if not path:
            return None

        parent_id = await directory_store.get_directory_id(user_id=user.user_id, path=path, name=name)
        LOGGER.debug(response_msg_format(get_request_id(request), "add_directory", directory_id=parent_id))

    # Generate an ID
    directory_id = uuid.uuid4()

    LOGGER.info(request_msg_format(get_request_id(request), "add_directory", user_id=user.user_id,
                                   directory_id=directory_id, name=name, path=path, parent_id=parent_id))
    directory = Directory(user.user_id, directory_id, parent_id, path=path, name=name)
    await directory_store.add_directory(user.user_id, directory)
    LOGGER.info(response_msg_format(get_request_id(request), "add_directory", directory_id=directory_id))
    await directory_store.commit()
    return directory_id


async def get_children(request: Request, **kwargs):
    directory_id = await get_directory_id(request, **kwargs)

    user = get_authenticated_user(request)
    store = get_directory_store(request)

    LOGGER.info(request_msg_format(get_request_id(request), "get_children", user_id=user.user_id,
                                   directory_id=directory_id))
    children = await store.get_children(user_id=user.user_id, directory_id=directory_id)
    LOGGER.info(response_msg_format(get_request_id(request), "get_children", children=[child.item_id for child in
                                                                                       children]))

    return children


async def move_directory(request: Request, **kwargs):
    user = get_authenticated_user(request)
    store = get_directory_store(request)
    path = extract(kwargs, "path", str)
    name = extract(kwargs, "name", str)
    directory_id = parse_uuid(extract(kwargs, "directory_id", str))
    destination_id = parse_uuid(extract(kwargs, "destination_id", str))

    LOGGER.info(request_msg_format(get_request_id(request), "move_directory", user_id=user.user_id,
                                   path=path, directory_id=directory_id, gateway_method=name))

    if directory_id:
        directory = await store.get_directory(user.user_id, directory_id=directory_id)
        if not directory:
            raise InvalidDirectory(directory_id)
        path = directory.path
        name = directory.name

    if not directory_id:
        directory_id = await store.get_directory_id(user.user_id, path, name)

        # Ensure that the path really matches with the directory ID in the case that the path is '/'
        # In that case, get_directory_id should return None which may be problematic if the path is simply not found
        if not directory_id and (path != "/" or name != ""):
            raise InvalidDirectory(path, name)

    new_directory = Directory(user.user_id, directory_id, destination_id, path, name)

    return await store.move_directory(user_id=user.user_id, directory_id=directory_id, directory=new_directory)


async def rename_directory(request: Request, **kwargs):
    user = get_authenticated_user(request)
    store = get_directory_store(request)
    name = extract(kwargs, "name", str)
    directory_id = await get_directory_id(request, **kwargs)
    return await store.rename_directory(user_id=user.user_id, directory_id=directory_id, name=name)


async def get_directory_items(request: Request, **body):
    user = get_authenticated_user(request)
    store = get_directory_store(request)
    note_store = get_note_store(request)
    article_note_store = get_article_note_store(request)
    directory_id = await get_directory_id(request, **body)
    user_id = user.user_id
    body["directory_id"] = directory_id
    return {
        "notes": await note_store.get_directory_note_items(user_id, directory_id),
        "articles": await article_note_store.get_directory_article_note_items(user_id, directory_id),
        "directories": await store.get_directory_items(user.user_id, directory_id)
    }
