import logging
import logging.handlers
import configparser
import os
import uuid

GATEWAY_LOGGER = "gateway"
DATABASE_LOGGER = "database"
SEARCH_LOGGER = "search"
API_LOGGER = "api"
AUTHENTICATION_LOGGER = "authentication"
RESOURCE_LOGGER = "resource"
KERNEL_LOGGER = "kernel"
MIDDLEWARE_LOGGER = "middleware"

LOGGERS = [GATEWAY_LOGGER, DATABASE_LOGGER, SEARCH_LOGGER, API_LOGGER, AUTHENTICATION_LOGGER, RESOURCE_LOGGER,
           KERNEL_LOGGER, MIDDLEWARE_LOGGER]

BASE_DIRECTORY = "./"

LOGGING_FORMATTER = logging.Formatter("%(asctime)s - %(name)s [%(levelname)s] %(message)s")


def request_msg_format(request_id: uuid.UUID, gateway_method: str, **parameters):
    parameter_string = ""
    count = 0
    parameter_length = len(parameters)
    for parameter in parameters:
        if count == parameter_length - 1:
            parameter_string += f"{parameter}:{parameters[parameter]}"
        else:
            parameter_string += f"{parameter}:{parameters[parameter]} , "
        count += 1
    return f"request: {request_id.hex:<16s} | {gateway_method:<30s} | ({parameter_string}))"


def response_msg_format(request_id: uuid.UUID, name: str, **response):
    if response:
        return f"response: {request_id.hex:<16s} | {name}"
    else:
        parameter_string = ""
        count = 0
        parameter_length = len(response)
        for parameter in response:
            if count == parameter_length - 1:
                parameter_string += f"{parameter}:{response[parameter]}"
            else:
                parameter_string += f"{parameter}:{response[parameter]} , "
            count += 1
        return f"response: {request_id.hex:<16s} | {name:<30s} | ({parameter_string}))"


def error_msg_format(request_id: uuid.UUID, err):
    return f"error: {request_id.hex} | {str(err)}"


def configure(file: str):
    configuration = configparser.ConfigParser(allow_no_value=True)
    with open(file, "r+") as f:
        configuration.read_file(f)
    global BASE_DIRECTORY
    BASE_DIRECTORY = configuration.get("logging", "directory", fallback="./")
    global LOGGERS
    for logger in LOGGERS:
        configure_logger(logger, configuration)


def configure_logger(name: str, config: configparser.ConfigParser):
    logger = logging.getLogger(name)
    logger.setLevel(config.get(name, 'level', fallback=logging.INFO))
    global BASE_DIRECTORY
    filename = os.path.join(BASE_DIRECTORY, config.get(name, 'file', fallback=f"{name}.log"))
    handler = logging.handlers.RotatingFileHandler(filename,
                                                   maxBytes=1280000)
    handler.setFormatter(LOGGING_FORMATTER)
    logger.handlers = []
    logger.addHandler(handler)
