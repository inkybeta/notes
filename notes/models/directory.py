import uuid
from typing import Optional


class Directory:
    def __init__(self, user_id: uuid.UUID, directory_id: Optional[uuid.UUID], parent_id: Optional[uuid.UUID],
                 path: str, name: str):
        self.user_id = user_id
        self.directory_id = directory_id
        self.parent_id = parent_id
        self.path = path
        self.name = name
