import uuid
import datetime
from typing import Optional


class Item:
    def __init__(self, item_id: uuid.UUID, title: str, parent: uuid.UUID,
                 timestamp: Optional[datetime.datetime] = None):
        self.item_id = item_id
        self.title = title
        self.parent = parent
        self.timestamp = timestamp


class SearchItem(Item):
    def __init__(self, item_id: uuid.UUID, title: str, parent: uuid.UUID,
                 highlights: [str],
                 timestamp: Optional[datetime.datetime] = None):
        self.highlights = highlights
        Item.__init__(self, item_id, title, parent, timestamp)
