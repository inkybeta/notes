import datetime
import uuid

from .update import NoteUpdate, Edit
from typing import List, Optional


class NoteInformation:
    def __init__(self, number_of_commits: int, oldest: datetime.datetime):
        self.commit_count = number_of_commits
        self.earliest_edit_time = oldest


class Note:
    def __init__(self, note_id: uuid.UUID, user_id: uuid.UUID, title: str, content: str, timestamp: datetime.datetime,
                 created: datetime.datetime, tags: [str], directory_id: Optional[uuid.UUID] = None):
        """
        Creates a note model with the specified parameters.
        :param note_id: the id of the note
        :param user_id: the id of the user
        :param title: the title of the note
        :param content: the body content of the text
        :param timestamp: the time at which the note was created in UTC time
        :param tags: a list of tags to associate with these notes
        """
        self.note_id = note_id
        self.user_id = user_id
        self.title = title
        self.content = content
        self.created = created
        self.tags = tags
        self.timestamp = timestamp
        self.directory_id = directory_id

    def apply_update(self, note_update: NoteUpdate) -> None:
        if note_update.title_update:
            self.title = note_update.title_update
        self.timestamp = note_update.timestamp
        if note_update.text_updates:
            for update in note_update.text_updates:
                if update.action == Edit.INSERTION:
                    self.content = NoteUpdate.insert(update.index,
                                                     self.content, update.text)
                elif update.action == Edit.DELETION:
                    self.content = NoteUpdate.delete(
                        update.index, self.content, len(update.text))
                elif update.action == Edit.REPLACEMENT:
                    self.content = NoteUpdate.replace(
                        update.index, self.content, update.text
                    )

    def apply_note(self, note: "Note"):
        self.title = note.title
        self.content = note.content
        self.timestamp = note.timestamp


class ArticleNote(Note):
    def __init__(self, note_id: uuid.UUID, user_id: uuid.UUID, title: str, summary: str, timestamp: datetime.datetime,
                 created: datetime.datetime, tags: List[str], url: str, directory_id: Optional[uuid.UUID],
                 published: datetime.datetime = None, authors: List[str] = None):
        Note.__init__(self, note_id, user_id, title, summary, timestamp, created, tags, directory_id=directory_id)
        self.url = url
        self.authors = authors
        self.published = published

    def apply_note(self, note: Note):
        if isinstance(note, ArticleNote):
            self.url = note.url
        Note.apply_note(self, note)

    def apply_update(self, update: NoteUpdate):
        if "url" in update.named_changes:
            self.url = update.named_changes["url"]
        if "authors" in update.named_changes:
            self.authors = update.named_changes["authors"]
        Note.apply_update(self, update)
