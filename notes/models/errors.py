from notes.models.response import StandardResponse


class StandardError(StandardResponse, BaseException):
    def __init__(self, code: int, error: str, reason: str, friendly: str = None):
        StandardResponse.__init__(self, False, code, error, reason, friendly, None)
        BaseException.__init__(self)


class ClientError(StandardError):
    def __init__(self, error: str, reason: str):
        StandardError.__init__(self, 400, error, reason, "The thing you are using is broken. Try going online, "
                                                         "or if you are already online, try refreshing. If that doesn't"
                                                         " work contact the developers. Thanks.")


class BodyMissingError(ClientError):
    def __init__(self):
        ClientError.__init__(self, "client/model/missing", "You are missing a body in a place that needs it")


class MissingParameterError(ClientError):
    def __init__(self, parameter: str):
        ClientError.__init__(self, "client/model/missing", f"Missing parameter {parameter}")


class UnexpectedParameterValue(ClientError):
    def __init__(self, parameter, value):
        ClientError.__init__(self, "client/model/unexpected", f"Unexpected parameter value {parameter}:{value}")


class ModelTypeError(ClientError):
    def __init__(self, parameter: str, expected_type: str = None):
        if expected_type:
            reason = f"{parameter} was not of the correct type. The correct type is {expected_type}"
        else:
            reason = f"{parameter} was not of the correct type. Please look at the documentation for more information."
        ClientError.__init__(self, "client/model/type", reason)


class ModelInputError(StandardError):
    def __init__(self, msg):
        StandardError.__init__(self, 400, "The user messed up.", msg)


class UUIDError(ModelTypeError):
    def __init__(self):
        ModelTypeError.__init__(self, "There was a problem parsing the UUID given. Please be sure to give "
                                      "the UUID as a hexadecimal string.", "uuid")


class EmailParseError(ModelTypeError):
    def __init__(self):
        ModelTypeError.__init__(self, "The email given was invalid. Please be sure to send a valid email.", "email")


class InvalidAuthorizationTokenError(StandardError):
    def __init__(self):
        StandardError.__init__(self, 400, "auth/token/invalid", "Invalid authorization token.")
