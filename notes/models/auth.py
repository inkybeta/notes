import uuid
import enum
import typing


class AuthenticationType(enum.Enum):
    FIREBASE = 1
    LOCAL = 0


class AuthenticatedUser:
    def __init__(self, authenticated: bool, user_id: uuid.UUID = None, verified: bool = False):
        self.authenticated = authenticated
        self.user_id = user_id
        self.verified = verified


class User:
    def __init__(self, user_id: uuid.UUID, email: str, first_name: str, middle_name: typing.Union[str, None],
                 last_name: str, authentication_type: AuthenticationType):
        self.user_id = user_id
        self.email = email
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.authentication_type = authentication_type
