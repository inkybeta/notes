from typing import List


class StandardResponse:
    def __init__(self, success: bool, code: int, error: str, reason: str, friendly: str, payload,
                 warnings: List[str] = None):
        self.success = success
        self.code = code
        self.error = error
        self.warnings = warnings if warnings else []
        self.friendly = friendly
        self.reason = reason
        self.payload = payload


