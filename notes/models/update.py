import json
import uuid
from datetime import datetime
from typing import List, Union


class Edit:
    INSERTION: int = 0
    DELETION: int = 1
    REPLACEMENT: int = 2

    def __init__(self, action: int, index: int, text: str = None):
        self.action = action
        self.index = index
        self.text = text

    def is_delete(self) -> bool:
        return self.action == Edit.DELETION

    def is_insertion(self) -> bool:
        return self.action == Edit.INSERTION


class NoteUpdate:
    def __init__(self, user_id: uuid.UUID, note_id: uuid.UUID, edit_id: uuid.UUID, timestamp: datetime,
                 tags: List[str] = None,
                 title_update: str = None,
                 text_updates: Union[List[Edit], str] = None, **kwargs):
        """
        Creates a model for NoteUpdate which can be applied to update a note.
        :param title_update: the updated title of the note if the title is updated
        :param text_updates: a list of updates to the text if the content is updated. the updates will
            be applied in order
        """
        self.note_id = note_id
        self.edit_id = edit_id
        self.user_id = user_id
        self.tags = tags
        self.timestamp = timestamp
        self.title_update = title_update
        if type(text_updates) is str:
            self.load_update_json(text_updates)
        else:
            self.text_updates = text_updates
        self.named_changes = kwargs

    @staticmethod
    def insert(index: int, source: str, insertion: str) -> str:
        return source[:index] + insertion + source[index:]

    @staticmethod
    def delete(index: int, source: str, char_count: int):
        return source[:index] + source[index + char_count:]

    @staticmethod
    def replace(index: int, source: str, replacement: str) -> str:
        return source[:index] + replacement + source[index + len(replacement):]

    def get_text_updates(self) -> str:
        return json.dumps({
            "text_updates": [{"insert": x.action, "index": x.index, "text": x.text} for x in self.text_updates]
        })

    def get_named_changes(self) -> str:
        return json.dumps(self.named_changes)

    def load_update_json(self, update: str):
        r = json.loads(update)
        self.text_updates = []
        for update in r["text_updates"]:
            self.text_updates.append(
                (update["insert"], update["index"], update["text"]))

    def merge(self, update: "NoteUpdate"):
        former = update.text_updates
        latter = self.text_updates

        assert len(former) > 0
        assert len(latter) > 0

        # We first need to check out four cases which can be broken into two segments:
        # Do the last item of former and the first item of latter and the last item of
        # former match. Let's consider where we do not match former and latter types

        if former[len(former) - 1].action != latter[0].action:
            # We can simply start a different segment and append
            former.extend(latter)
            return NoteUpdate(self.user_id, self.note_id, uuid.uuid4(), self.timestamp, self.tags, self.title_update,
                              former, **self.named_changes)

        element = former[0].action

        def find_right_most() -> int:
            """
            Finds the right boundary of the two arrays to be merged. This should
            occur in latter and defines the number of elements that we can go up to.
            Not inclusive of the last element
            """
            # We should prob just precompute this when we are marking the deletion.
            # Something simple like min(PartialUpdate.deletion, currentIndexOfDeletion)
            # Return the length of the array if there is no deletion in the latter
            nonlocal element
            for i in range(len(latter)):
                if latter[i].action != element:
                    return i
            return len(latter)

        def find_left_most() -> int:
            """
            Finds the left boundary of the two arrays to be merged. This should occur
            in the former and defines the number of elements in the previous array that we
            should start at
            """
            # We should prob just precompute this as well. However, this would require an
            # arm and a leg rn so we should just do a bit of clean up
            nonlocal element
            length = len(former)
            for i in range(length):
                if former[length - i - 1].action != element:
                    return i + 1
            return 0

        left = find_left_most()
        right = find_right_most()

        former_index = left
        latter_index = 0

        def get_minimum(left: int, right: int) -> (Edit, bool):
            nonlocal former_index
            nonlocal latter_index
            nonlocal former
            nonlocal latter
            if latter_index == right or former[former_index].index <= latter[latter_index].index:
                m = former[former_index]
                former_index += 1
                return m, True
            elif former_index < len(former):
                m = latter[latter_index]
                latter_index += 1
                return m, False
            else:
                raise Exception("Should not reach here.")

        documentShift = 0
        edits = []
        for _ in range(right + (len(former) - left)):
            m, fr = get_minimum(left, right)

            if fr:
                m.index += documentShift

            # we can be a bit more clever here since we can decide to combine
            # continuous intervals of adds/deletes right after this by taking a
            # look at the last element

            # push the shifted edit in
            edits.append(m)
            if not fr:
                if m.action == Edit.INSERTION:
                    documentShift += len(m.text)
                elif m.action == Edit.DELETION:
                    documentShift -= len(m.text)
        total = former[0:left]
        total.extend(edits)
        total.extend(latter[right:len(latter)])
        print([str(m) for m in total])
        return NoteUpdate(self.user_id, self.note_id, uuid.uuid4(), datetime.now(), text_updates=total)
